/**
 * 
 */
package nl.bori.calweb.persistence.measurements.service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author ridderdm
 *
 */
class ListUtils<K> {

	/**
	 * Creates from the input string a new string that can be used to make an
	 * getter method.
	 * 
	 * @param string 
	 * @return same string in camelCase
	 */
	static String camelCase(String string) {
		if (string != null && (string = string.trim()).length() > 0) {
			string = string.substring(0,1).toUpperCase() + string.substring(1);
		}
		return string;
	}
	
	/**
	 * Creates a getter method name for a class field
	 * 
	 * @param field the field for which to create a getter method
	 * 
	 * @return string which is the name of the getter for field
	 */
	static String convertFieldToAccessor(String field) {
		return "get" + camelCase(field);
	}
	
	
	/**
	 * Convert a List into a HashMap with the designated field as key.
	 * 
	 * @param <V>
	 * @param list the list to convert
	 * @param keyField the field of the objects in list to use as key in the resulting map
	 * @return the map with fields field and objects from list
	 * 
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@SuppressWarnings("unchecked")
	<V> Map<K, V> toMap(List<V> list, String keyField) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		
		String accessor = convertFieldToAccessor(keyField);
		Map<K, V> map = new HashMap<K, V>();
		for (V object : list) {
			Method method = object.getClass().getDeclaredMethod(accessor);
			K key = (K)method.invoke(object);
			map.put(key, object);
		}
		return map;
	}
}

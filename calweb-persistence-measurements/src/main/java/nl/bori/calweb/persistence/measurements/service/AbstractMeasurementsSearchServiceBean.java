/**
 * $Id$
 */
package nl.bori.calweb.persistence.measurements.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import nl.bori.calweb.services.measurementsservice.MeasurementsSearchService;
import nl.bori.calweb.services.measurementsservice.model.Meetgegevens;
import nl.bori.calweb.services.measurementsservice.model.Monster;
import nl.bori.calweb.services.measurementsservice.model.Substance;
import nl.bori.calweb.services.measurementsservice.search.LocationSearchControls;
import nl.bori.calweb.services.measurementsservice.search.MeetgegevensSearchControls;
import nl.bori.calweb.services.retrieval.RetrievalException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author ridderdm
 * 
 */
public abstract class AbstractMeasurementsSearchServiceBean implements
		MeasurementsSearchService {

	protected Logger logger = Logger
			.getLogger(AbstractMeasurementsSearchServiceBean.class);

	private final static String defaultDatetimePattern = "dd-MM-YYYY HH:mm"; // TODO

	// uit
	// properties
	// halen

	/**
	 * 
	 */
	public AbstractMeasurementsSearchServiceBean() {
		logger.debug("Constructor aangeroepen.");
	}

	/**
	 * @see nl.bori.calweb.services.retrieval.RetrievalService#search(nl.bori.calweb.services.retrieval.SearchControls)
	 * 
	 *      De search() gebruikt voor niet ingevulde parameters van de search
	 *      controls defaults. Deze defaults zijn als volgt:
	 *      <ul>
	 *      <li>datum van ontvangst moet liggen binnen een maand eindigend
	 *      'morgen'</li>
	 *      <li>monstername dag moet eindigen voor 'morgen' en starten na het
	 *      jaar 1500</li>
	 *      <li>meting moet uitgevoerd zijn in een periode die 'morgen' eindigt
	 *      en een maand eerder begint
	 *      <li>nuclides zijn alle bekende nucliden
	 *      </ul>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Monster> search(MeetgegevensSearchControls searchControls)
			throws RetrievalException {

		DateTimeFormatter fmt = DateTimeFormat
				.forPattern(defaultDatetimePattern);

		logger.debug("Search met controls : " + searchControls.toString());

		if (getEntityManager() == null) {
			logger
					.fatal("EntityManager is null, toegang tot de database is niet mogelijk; kan niet verder gaan.");
			throw new RetrievalException(
					"Toegang tot de 'stcmetingen' database niet mogelijk.");
		}

		if (!"".equals(searchControls.getIdentifier())
				&& searchControls.getIdentifier() != null) {

			/*
			 * Als een identifier meegegeven is voor de Meetgegevens, zoeken we
			 * direct hiermee.
			 * 
			 * De identifier uit de searchControls is een String, terwijl wij
			 * een integer nodig hebben: conversie
			 */

			if (StringUtils.isNumeric(searchControls.getIdentifier())) {

				return getEntityManager().find(Meetgegevens.class,
						new Integer(searchControls.getIdentifier()))
						.getMonsters();
			}
		}

		Query query = getEntityManager()
				.createQuery(
						"select distinct monsters from "
								+ "				Monster monsters, "
								+ "			  	IN(monsters.meetgegevens) meetgegevens, "
								+ "				IN(monsters.bepalings) bepalings,"
								+ "				IN(monsters.substance) substances, "
								+ "             IN(monsters.locatie) locatie"
								+ " where "
								+ "("
								+ getProjectIdClause(searchControls)
								+ getLocationClause(searchControls)
								+ " monsters.monsterBegin > :monsternameBegin "
								+ "and monsters.monsterEind < :monsternameEnd "
								+ "and meetgegevens.datumOntvangst between :datumOntvangst_start and :datumOntvangst_end) "
								+ "and ("
								+ "bepalings.nuclide in (:nuclidelist) "
								+ " or bepalings.opgestuurdeNuclide in (:nuclidelist) "
								+ ") "
								+ "and substances in (:substancelist) "
								+ "and bepalings.bepalingBegin > :bepalingBegin "
								+ "and bepalings.bepalingEind < :bepalingEind "
								+ ")");

		/* het project waartoe de meetgegevens moeten horen */
		if (searchControls.getProjectId() == null) {
			logger
					.debug("Geen projectidentificatie meegestuurd bij het zoeken naar metingen.");
		} else {
			logger
					.debug("Zending moet ingestuurd zijn voor project met identificatie "
							+ (new Integer(searchControls.getProjectId()))
									.intValue());
			query.setParameter("projectId", (new Integer(searchControls
					.getProjectId())).intValue());
		}
		
		/* locatie */
		if (searchControls.getMonsterSearchControls() != null &&
				searchControls.getMonsterSearchControls().getLocationSearchControls() != null) {
			
			LocationSearchControls lsc = searchControls.getMonsterSearchControls().getLocationSearchControls();
			
			query.setParameter("xmin", lsc.getLocationXBegin());
			query.setParameter("xmax", lsc.getLocationXEnd());
			query.setParameter("ymin", lsc.getLocationYBegin());
			query.setParameter("ymax", lsc.getLocationYEnd());
			
		}
		
		
		/* periode waarin het monster genomen mag zijn */

		/* begin van de periode van monstername */
		DateTime monsternameStartsAfter = searchControls
				.getMonsterSearchControls().getStartDate();
		if (monsternameStartsAfter == null) {
			monsternameStartsAfter = (new DateTime()).minusYears(400)
					.withHourOfDay(0).withMinuteOfHour(0).withMinuteOfHour(0);
		}
		logger.debug("Monster moet genomen zijn na "
				+ fmt.print(monsternameStartsAfter));
		query.setParameter("monsternameBegin", monsternameStartsAfter
				.toCalendar(null), TemporalType.TIMESTAMP);

		/* einde van de periode van monstername */
		DateTime monsternameEndsBefore = searchControls
				.getMonsterSearchControls().getEndDate();
		if (monsternameEndsBefore == null) {
			/*
			 * omdat de uren bij deze selectie meetellen, moeten we de dag
			 * component een dagje later zetten.
			 */
			monsternameEndsBefore = (new DateTime()).plusDays(1).withHourOfDay(
					0).withMinuteOfHour(0);

		}
		logger.debug("Monster moet genomen zijn voor "
				+ fmt.print(monsternameEndsBefore));
		query.setParameter("monsternameEnd", monsternameEndsBefore
				.toCalendar(null), TemporalType.TIMESTAMP);

		/* periode van ontvangst */

		/* eind van de periode */
		DateTime periodOfReceival_End = searchControls.getDateOfUploadEnd();
		if (periodOfReceival_End == null) {

			/*
			 * eind datum wordt huidige datum & tijd waar we een dag bij op
			 * tellen om zeker te weten dat we alles hebben
			 */
			periodOfReceival_End = (new DateTime()).plusDays(1)
					.withHourOfDay(0).withMinuteOfHour(0);

		}
		logger.debug("Zending moet ontvangen zijn voor "
				+ fmt.print(periodOfReceival_End));
		query.setParameter("datumOntvangst_end", periodOfReceival_End
				.toCalendar(null), TemporalType.TIMESTAMP);

		/* begin van de periode */
		DateTime periodOfReceival_Start = searchControls.getDateOfUploadStart();
		if (periodOfReceival_Start == null) {

			/*
			 * als de startdatum leeg is, gebruiken we de eind datum en creeren
			 * we een periode van een maand
			 */

			periodOfReceival_Start = (new DateTime(periodOfReceival_End))
					.minusMonths(1).withHourOfDay(0).withMinuteOfHour(0);

		}

		logger.debug("Zending moet ontvangen zijn na "
				+ fmt.print(periodOfReceival_Start));
		query.setParameter("datumOntvangst_start", periodOfReceival_Start
				.toCalendar(null), TemporalType.TIMESTAMP);

		/* parameters voor de datum en tijd van het uitvoeren van een bepaling */

		/* eind van de periode */
		DateTime periodOfMeasurement_End = searchControls
				.getMonsterSearchControls().getBepalingSearchControls()
				.getEndDate();
		if (periodOfMeasurement_End == null) {

			/*
			 * eind datum wordt huidige datum & tijd waar we een dag bij op
			 * tellen om zeker te weten dat we alles hebben
			 */
			periodOfMeasurement_End = (new DateTime()).plusDays(1)
					.withHourOfDay(0).withMinuteOfHour(0);

		}
		logger.debug("Meting moet uitgevoerd zijn voor "
				+ fmt.print(periodOfMeasurement_End));
		query.setParameter("bepalingEind", periodOfMeasurement_End
				.toCalendar(null), TemporalType.TIMESTAMP);

		/* begin van de periode */
		DateTime periodOfMeasurement_Start = searchControls
				.getMonsterSearchControls().getBepalingSearchControls()
				.getStartDate();
		if (periodOfMeasurement_Start == null) {
			periodOfMeasurement_Start = new DateTime(periodOfMeasurement_End)
					.minusYears(400).withHourOfDay(0).withMinuteOfHour(0);
		}
		logger.debug("Meting moet uitgevoerd zijn na "
				+ fmt.print(periodOfMeasurement_Start));
		query.setParameter("bepalingBegin", periodOfMeasurement_Start
				.toCalendar(null), TemporalType.TIMESTAMP);

		/*
		 * zoeken naar nucliden
		 */
		if (searchControls.getMonsterSearchControls()
				.getBepalingSearchControls().getNuclide().size() != 0) {
			logger.debug("Nuclide moet er een zijn uit de meegegeven lijst "
					+ searchControls.getMonsterSearchControls()
							.getBepalingSearchControls().getNuclide());
			query.setParameter("nuclidelist", searchControls
					.getMonsterSearchControls().getBepalingSearchControls()
					.getNuclide());
		} else {
			List<String> avNuclides = this.getQuantities();
			logger.debug("Nuclide moet er een zijn uit de beschikbare lijst "
					+ avNuclides);

			/*
			 * Volgens de JPA 1.0 specificatie moet een lijst die gebruikt wordt
			 * bij het zoeken minimaal 1 element bevatten. Is de lijst leeg, dan
			 * krijg je bij het uitvoeren van de query een foutmelding.
			 * 
			 * Om de foutmelding te vermijden is het volgens diverse bronnen op
			 * internet het eenvoudigst zelf te controleren op null-lijsten. Wij
			 * doen het vrij lomp omdat we niet zelf de query willen verbouwen.
			 * Omdat de lijst geen nuclide elementen bevat is bekend dat de
			 * tabellen geen metingen bevatten. We kunnen dus ieder gewenste
			 * tekst in de lijst stoppen: er komt toch geen meting uit de query.
			 * 
			 * In JPA 2.0 schijnt dit anders te werken.
			 */

			if (avNuclides.size() == 0) {
				avNuclides.add("EenNietBestaandNuclide");
			}

			query.setParameter("nuclidelist", avNuclides);
		}

		/*
		 * Zoeken naar substances
		 */
		if (searchControls.getMonsterSearchControls().getSubstanceCodes()
				.size() != 0) {
			logger
					.debug("Substance moet er een zijn uit de lijst met de volgende codes : "
							+ searchControls.getMonsterSearchControls()
									.getSubstanceCodes());

			List<Substance> substances = new ArrayList<Substance>();

			try {
				Map<String, Substance> map = new ListUtils<String>().toMap(this
						.getSubstances(), "eurdepCode");
				Iterator<String> it = searchControls.getMonsterSearchControls()
						.getSubstanceCodes().iterator();
				while (it.hasNext()) {
					substances.add(map.get(it.next()));
				}
			} catch (SecurityException e) {
				logger
						.error("During converson of the list of substances to hashmap an exception was thrown : "
								+ e.getMessage());
			} catch (IllegalArgumentException e) {
				logger.error(e.getMessage());
			} catch (NoSuchMethodException e) {
				logger.error(e.getMessage());
			} catch (IllegalAccessException e) {
				logger.error(e.getMessage());
			} catch (InvocationTargetException e) {
				logger.error(e.getMessage());
			}

			query.setParameter("substancelist", substances);
			logger.debug("Substance moet er een zijn uit de lijst : "
					+ substances);

		} else {

			List<Substance> availableSubstance = this.getSubstances();

			logger.debug("Substance moet er een zijn uit de lijst : "
					+ availableSubstance);

			/*
			 * Er moeten altijd substances zijn, we hoeven daarom niet te
			 * controleren op een lege lijst..
			 */

			query.setParameter("substancelist", availableSubstance);

		}

		/* en nu zoeken */

		List<Monster> resultList = query.getResultList();
		logger.debug("Aantal gevonden metingen : " + resultList.size());
		return resultList;
	}

	private String getLocationClause(MeetgegevensSearchControls searchControls) {
		
		if (searchControls.getMonsterSearchControls() == null) return "";
		
		if (searchControls.getMonsterSearchControls().getLocationSearchControls() == null) return "";
		
		return " (   locatie.XCoordinaat < :xmax " +
				" and locatie.XCoordinaat > :xmin " +
				" and locatie.YCoordinaat < :ymax " +
				" and locatie.YCoordinaat > :ymin) and ";

	}

	private String getProjectIdClause(MeetgegevensSearchControls searchControls) {
		
		if (searchControls.getProjectId() != null) {
			return " meetgegevens.idCalwebstatus = :projectId and ";
		} else {
			return "";
		}
		
	}

	/**
	 * @see
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getQuantities() {

		Query query = getEntityManager()
				.createQuery(
						"select distinct bepaling.nuclide from Bepaling bepaling where nuclide != '' ");
		List<String> result = query.getResultList();

		query = getEntityManager()
				.createQuery(
						"select distinct bepaling.opgestuurdeNuclide from Bepaling bepaling where nuclide = '' ");
		result.addAll(query.getResultList());

		return result;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Substance> getSubstances() {
		Query query = getEntityManager().createQuery(
				"select substance from Substance substance");
		return query.getResultList();
	}

	/**
	 * Return the entitymanager used by the methods
	 * 
	 * @return the entitymanager
	 */
	public abstract EntityManager getEntityManager();

}

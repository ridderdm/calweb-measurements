/**
 * $Id:$
 */
package nl.bori.calweb.persistence.measurements.service.helper;

import javax.persistence.EntityManager;

import nl.bori.calweb.persistence.measurements.service.AbstractMeasurementsSearchServiceBean;

/**
 * @author ridderdm
 *
 */
public class MeasurementsSearchServiceBean extends
		AbstractMeasurementsSearchServiceBean {

	private EntityManager em;

	
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	/* (non-Javadoc)
	 * @see nl.bori.calweb.persistence.measurements.service.AbstractMeasurementsSearchServiceBean#getEntityManager()
	 */
	@Override
	public EntityManager getEntityManager() {
		return em;
	}

}

/**
 * $Id$
 */
package nl.bori.calweb.persistence.measurements.service;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import nl.bori.calweb.persistence.measurements.service.helper.MeasurementsSearchServiceBean;
import nl.bori.calweb.services.measurementsservice.search.LocationSearchControls;
import nl.bori.calweb.services.measurementsservice.search.MeetgegevensSearchControls;
import nl.bori.calweb.services.measurementsservice.search.MonsterSearchControls;
import nl.bori.calweb.services.measurementsservice.search.BepalingSearchControls;
import nl.bori.calweb.services.measurementsservice.model.Monster;
import nl.bori.calweb.services.retrieval.RetrievalException;

import static org.junit.Assert.*;

import org.dbunit.DatabaseUnitException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.MutableDateTime;
import org.junit.Before;
import org.junit.Test;


/**
 * Tests for the search function in the database with measurements.
 * 
 * @author ridderdm
 *
 */
public class MeasurementsSearchServiceTest extends MeasurementsServiceAbstractTest {

	private MeasurementsSearchServiceBean bean;

	private static int allSamplesCount = 42;
	
	@Before
	public void setUp() throws FileNotFoundException, ClassNotFoundException, DatabaseUnitException, SQLException {
		
		logger.info("setUp() van de entitymanager(factory) en de bean.");
		
		prepareDatabase();
		
		bean = new MeasurementsSearchServiceBean();
		bean.setEntityManager(getEntityManager());
	}
	
	@Test
	public void searchWithNoControls() throws RetrievalException {
		
		/*
		 * De ingevoerde metingen zijn van begin juni 2010 dus mogen nooit gevonden
		 * worden.
		 */
		
		
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(0, resultList.size());	
		
		searchControls.setProjectId("0");
		resultList = bean.search(searchControls);
		assertEquals(0, resultList.size());	
		
		searchControls.setProjectId("254");
		resultList = bean.search(searchControls);
		assertEquals(0, resultList.size());	

	}
	
	/*
	 * 
	 * De test op zoeken met monstername gegevens. 
	 * 
	 * De bean gebruikt defaults voor het zoeken op tijdstip van upload, om deze test
	 * op het zoeken met monstergegevens goed uit te voeren, moeten we het zoeken op
	 * de upload tijd "uitschakelen" door een zeer grote periode in te voeren waarin
	 * alle opgestuurde metingen vallen.  
	 */
	@Test 
	public void searchWithSampleDateLimits() throws RetrievalException {

		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		searchControls.setProjectId("254");
		
		MutableDateTime nextMonth = new MutableDateTime(); // nextMonth is today
		nextMonth.addMonths(1);

		searchControls.setDateOfUploadEnd(nextMonth.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);

		/*
		 * Zoek met begin datum voor monstername
		 */
		
		MonsterSearchControls monsterControls = new MonsterSearchControls();
		MutableDateTime year2008 = new MutableDateTime();
		year2008.setDayOfMonth(1);
		year2008.setMonthOfYear(DateTimeConstants.JANUARY);
		year2008.setYear(2008);
		monsterControls.setStartDate(year2008.toDateTime());
		searchControls.setMonsterSearchControls(monsterControls);
		
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(16, resultList.size());
		
		/*
		 * Zoek met eind datum voor monstername
		 */
		
		MutableDateTime year2009 = new MutableDateTime();
		year2009.setDayOfMonth(1);
		year2009.setMonthOfYear(DateTimeConstants.JANUARY);
		year2009.setYear(2009);
		monsterControls.setEndDate(year2009.toDateTime());
		searchControls.setMonsterSearchControls(monsterControls);
		
		resultList = bean.search(searchControls);
		assertEquals(15, resultList.size());	

	}
	
	/*
	 * 
	 * De test op zoeken met bepaling gegevens. 
	 * 
	 * De bean gebruikt defaults voor het zoeken op tijdstip van upload, om deze test
	 * op het zoeken met monstergegevens goed uit te voeren, moeten we het zoeken op
	 * de upload tijd "uitschakelen" door een zeer grote periode in te voeren waarin
	 * alle opgestuurde metingen vallen.  
	 */
	@Test 
	public void searchWithMeasurementDateLimits() throws RetrievalException {

		/*
		 * Maak een control voor zoeken op bepaling en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		searchControls.setProjectId("254");
		
		MutableDateTime nextMonth = new MutableDateTime(); // nextMonth is today
		nextMonth.addMonths(1);
		
		searchControls.setDateOfUploadEnd(nextMonth.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);
		
		MonsterSearchControls sampleControls = new MonsterSearchControls();
		
		BepalingSearchControls bepalingControls = new BepalingSearchControls();
		
		/*
		 * Test wat er gebeurd als je geen controls opgeeft
		 */
		sampleControls.setBepalingSearchControls(bepalingControls);
		searchControls.setMonsterSearchControls(sampleControls);
		
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(allSamplesCount, resultList.size());	
		
		/*
		 * Nu een einddatum opgeven
		 */
		
		MutableDateTime endDate = new MutableDateTime();
		endDate.setYear(2007);
		endDate.setMonthOfYear(DateTimeConstants.DECEMBER);
		endDate.setDayOfMonth(31);
		
		
		bepalingControls.setEndDate(endDate.toDateTime());
		
		sampleControls.setBepalingSearchControls(bepalingControls);
		searchControls.setMonsterSearchControls(sampleControls);		
		
		resultList = bean.search(searchControls);
		assertEquals(26, resultList.size());
		
		/*
		 * Nu een begindatum toevoegen
		 */
		MutableDateTime beginDate = new MutableDateTime();
		beginDate.setYear(2007);
		beginDate.setMonthOfYear(DateTimeConstants.JANUARY);
		beginDate.setDayOfMonth(1);
		bepalingControls.setStartDate(beginDate.toDateTime());
		
		sampleControls.setBepalingSearchControls(bepalingControls);
		searchControls.setMonsterSearchControls(sampleControls);		
		
		resultList = bean.search(searchControls);
		assertEquals(4, resultList.size());		
		
	}

	
	
	@Test
	public void searchWithUploadDateLimits() throws RetrievalException {
		
		/*
		 * Alle metingen vallen onder project 254
		 */
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		searchControls.setProjectId("254");
		
		/*
		 * zet nu een startdatum waardoor een aantal metingen buiten de selectie vallen 
		 */
		MutableDateTime mutableStartDate = new MutableDateTime();
		mutableStartDate.setYear(2010);
		mutableStartDate.setMonthOfYear(DateTimeConstants.JUNE);
		mutableStartDate.setDayOfMonth(1);
		
		searchControls.setDateOfUploadStart(mutableStartDate.toDateTime());
		
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(29, resultList.size());
		
		/*
		 * en zet nu een startdatum waardoor alle metingen binnen de selectie vallen
		 */
		mutableStartDate.setMonthOfYear(DateTimeConstants.MAY);
		searchControls.setDateOfUploadStart(mutableStartDate.toDateTime());
		
		resultList = bean.search(searchControls);
		assertEquals(allSamplesCount, resultList.size());

		/*
		 * Zet nu een einddatum waardoor alle metingen binnen de selectie vallen
		 * De startdatum is nog steeds maart uit de vorige test
		 */

		MutableDateTime endDate = new MutableDateTime();
		endDate.setMonthOfYear(DateTimeConstants.JULY);
		endDate.setYear(2010);
		endDate.setDayOfMonth(1);
		searchControls.setDateOfUploadEnd(endDate.toDateTime());
		
		resultList = bean.search(searchControls);
		assertEquals(allSamplesCount, resultList.size());

		/*
		 * Wijzig nu de startdatum waardoor metingen buiten de selectie vallen.
		 * De startdatum is leeg
		 */
		searchControls.setDateOfUploadStart(null);
		
		resultList = bean.search(searchControls);
		assertEquals(29, resultList.size());


		searchControls = new MeetgegevensSearchControls();
		searchControls.setProjectId("254");

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);
		
		resultList = bean.search(searchControls);
		
		assertEquals(allSamplesCount, resultList.size());

	}

	
	@Test
	public void searchWithNuclides() throws RetrievalException {
	
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		searchControls.setProjectId("254");
		
		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);


		List<String> list = new ArrayList<String>();
		list.add("Cs-137");
		BepalingSearchControls bepalingControls = new BepalingSearchControls();
		bepalingControls.setNuclide(list);
		
		MonsterSearchControls sampleControls = new MonsterSearchControls();
		sampleControls.setBepalingSearchControls(bepalingControls);
		
		searchControls.setMonsterSearchControls(sampleControls);
		
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(11, resultList.size());

		/*
		 * nuclide toevoegen
		 */
		list.add("I-131");
		bepalingControls.setNuclide(list);
		
		sampleControls = new MonsterSearchControls();
		sampleControls.setBepalingSearchControls(bepalingControls);
		
		searchControls.setMonsterSearchControls(sampleControls);
		
		resultList = bean.search(searchControls);
		
		/*
		 * zowel I-131 als Cs-137 wordt gematched
		 */
		assertEquals(18, resultList.size());
		
	}
	
	
	@Test
	public void searchWithProjectId() throws RetrievalException {
		
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		
		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);
		
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(43, resultList.size());
		
		
		searchControls.setProjectId("254");
		resultList = bean.search(searchControls);
		assertEquals(42, resultList.size());
		
	}
	
	@Test
	public void testSearchWithSubstances() throws RetrievalException {
		
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		
		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);
		
		
		MonsterSearchControls sampleControls = new MonsterSearchControls();
		searchControls.setMonsterSearchControls(sampleControls);
		searchControls.setProjectId("254");
		
		List<String> substanceCodes = new ArrayList<String>();
		substanceCodes.add("A1");
		sampleControls.setSubstanceCodes(substanceCodes);
		
		List<Monster> resultList = bean.search(searchControls);
		assertEquals(3, resultList.size());

		/*
		 * De monsters met substance = C zitten in dezelfde meetgegevens als de monsters met substance A1,
		 * het resultaat van deze query is dus hetzelfde aantal meetgegevens.
		 */
		substanceCodes.add("C");
		sampleControls.setSubstanceCodes(substanceCodes);
		
		resultList = bean.search(searchControls);
		assertEquals(6, resultList.size());
		

		/*
		 * Het monster met substance = A5 zit in een meetgegevens dat nog niet door de vorige query wordt 
		 * teruggegeven.
		 */
		substanceCodes.add("A5");
		sampleControls.setSubstanceCodes(substanceCodes);
		
		resultList = bean.search(searchControls);
		assertEquals(10, resultList.size());

		
	}
	
	@Test
	public void testGetByIdentifier() throws RetrievalException {
		
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		searchControls.setIdentifier("5");
		
		List<Monster> list = bean.search(searchControls);
		
		assertEquals(22, list.size());
		assertEquals(Integer.valueOf(5) , list.get(0).getMeetgegevens().getId());
		
		
	}
	
	@Test
	public void testWithEmptyResultSet() throws RetrievalException {
		
		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		searchControls.setProjectId("1234567");
		
		assertEquals(0, bean.search(searchControls).size());
		
	}	
	
	@Test
	public void testWithLocationConstraints() throws RetrievalException {

		MeetgegevensSearchControls searchControls = new MeetgegevensSearchControls();
		MonsterSearchControls msc = new MonsterSearchControls();
		LocationSearchControls lsc = new LocationSearchControls();
		
		msc.setLocationSearchControls(lsc);
		searchControls.setMonsterSearchControls(msc);

		
		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);

		
		/*
		 * Selecteer alle metingen
		 */
		lsc.setLocationXBegin(0);
		lsc.setLocationYBegin(0);
		lsc.setLocationXEnd(1000000);
		lsc.setLocationYEnd(1000000);
		
		assertEquals(43, bean.search(searchControls).size());

		/*
		 * Selecteer een deel
		 */
		lsc.setLocationXBegin(0);
		lsc.setLocationYBegin(0);
		lsc.setLocationXEnd(100);
		lsc.setLocationYEnd(400);
		
		assertEquals(13, bean.search(searchControls).size());
		
		/*
		 * Selecteer niets
		 */
		lsc.setLocationXBegin(0);
		lsc.setLocationYBegin(0);
		lsc.setLocationXEnd(100);
		lsc.setLocationYEnd(300);
		
		assertEquals(0, bean.search(searchControls).size());
		
		
		
	}
	
}

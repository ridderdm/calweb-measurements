/**
 * 
 */
package nl.bori.calweb.persistence.measurements.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author ridderdm
 *
 */
public class ListUtilsTest {

	
	List<DigitAndText> digits = new ArrayList<DigitAndText>(4);

	@Before
	public void prepareTests() {
		digits.add(new DigitAndText(1, "een"));
		digits.add(new DigitAndText(2, "twee"));
		digits.add(new DigitAndText(3, "drie"));
		digits.add(new DigitAndText(4, "vier"));
	}

	@After
	public void clearTests() {
		digits.clear();
	}
	
	@Test
	public void testCamelCase() {
		
		String text = ListUtils.camelCase("aaaa");
		assertEquals("Aaaa", text);
		
		text = ListUtils.camelCase("aaaA");
		assertEquals("AaaA", text);

		text = ListUtils.camelCase("aaaA ");
		assertEquals("AaaA", text);

	}
	
	@Test
	public void testConvertFieldToAccessor() {

		String text = ListUtils.convertFieldToAccessor("aaaa");
		assertEquals("getAaaa", text);
		
		text = ListUtils.convertFieldToAccessor("aaaA ");
		assertEquals("getAaaA", text);	

	}
	
	@Test
	public void testToMapOnDigit() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		
		Map<Integer, DigitAndText> map = new ListUtils<Integer>().toMap(digits, "digit");
		
		assertEquals(digits.size(), map.size());
		assertEquals("twee", map.get(2).getText());
		
	}
	
	@Test
	public void testToMapOnText() throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		
		Map<String, DigitAndText> map = new ListUtils<String>().toMap(digits, "text");
		
		assertEquals(digits.size(), map.size());
		assertEquals(2, map.get("twee").getDigit());
	}
	

	/**
	 * Simple class to test the conversion from a list to a Hash
	 * 
	 * @author ridderdm
	 *
	 */
	private class DigitAndText {
		
		private int digit;
		private String text;
		
		public DigitAndText(int digit, String text) {
			setDigit(digit);
			setText(text);
		}
		
		public int getDigit() {
			return digit;
		}
		
		public void setDigit(int digit) {
			this.digit = digit;
		}
		
		public String getText() {
			return text;
		}
		
		public void setText(String text) {
			this.text = text;
		}
		
	}


	
	
}

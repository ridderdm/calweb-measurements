/**
 * $Id$
 */
package nl.bori.calweb.persistence.measurements.service;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import nl.bori.calweb.persistence.measurements.service.helper.MeasurementsSearchServiceBean;
import nl.bori.calweb.services.measurementsservice.model.Substance;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the utility function on the database with measurements
 * 
 * @author ridderdm
 *
 */
public class MeasurementsUtilTest extends MeasurementsServiceAbstractTest {

	
	private MeasurementsSearchServiceBean bean;
	
	@Before
	public void setUp() throws FileNotFoundException, ClassNotFoundException, DatabaseUnitException, SQLException {
		
		logger.info("setUp() van de entitymanager(factory) en de bean.");
		
		prepareDatabase();
		
		bean = new MeasurementsSearchServiceBean();
		bean.setEntityManager(getEntityManager());
	}
	
	@Test
	public void getNuclides() {
		
		List<String> resultList = bean.getQuantities();
		assertEquals(13, resultList.size());
		
	}
	
	@Test
	public void getSubstances() {
		
		List<Substance> resultList = bean.getSubstances();
		assertEquals(5, resultList.size());
		
		assertTrue(resultList.get(0) instanceof Substance);
	}
	
}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "monster_vwa")
public class MonsterVwa implements java.io.Serializable {

	private static final long serialVersionUID = -4981312200227360434L;

	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private Integer idMonster;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	private Monster monster;

	@Column(name = "registratienummer", nullable = false, length = 15)
	private String registratienummer;

	@Column(name = "soort_waar", nullable = false)
	private String soortWaar;

	@Column(name = "herkomst", nullable = false)
	private String herkomst;

	@Column(name = "gemeente_monstername", nullable = false)
	private String gemeenteMonstername;

	@Column(name = "analysemethode", nullable = false, length = 15)
	private String analysemethode;

	public MonsterVwa() {
	}

	public Integer getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(Integer idMonster) {
		this.idMonster = idMonster;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public String getRegistratienummer() {
		return this.registratienummer;
	}

	public void setRegistratienummer(String registratienummer) {
		this.registratienummer = registratienummer;
	}

	public String getSoortWaar() {
		return this.soortWaar;
	}

	public void setSoortWaar(String soortWaar) {
		this.soortWaar = soortWaar;
	}

	public String getHerkomst() {
		return this.herkomst;
	}

	public void setHerkomst(String herkomst) {
		this.herkomst = herkomst;
	}

	public String getGemeenteMonstername() {
		return this.gemeenteMonstername;
	}

	public void setGemeenteMonstername(String gemeenteMonstername) {
		this.gemeenteMonstername = gemeenteMonstername;
	}

	public String getAnalysemethode() {
		return this.analysemethode;
	}

	public void setAnalysemethode(String analysemethode) {
		this.analysemethode = analysemethode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((analysemethode == null) ? 0 : analysemethode.hashCode());
		result = prime
				* result
				+ ((gemeenteMonstername == null) ? 0 : gemeenteMonstername
						.hashCode());
		result = prime * result
				+ ((herkomst == null) ? 0 : herkomst.hashCode());
		result = prime * result
				+ ((idMonster == null) ? 0 : idMonster.hashCode());
		result = prime
				* result
				+ ((registratienummer == null) ? 0 : registratienummer
						.hashCode());
		result = prime * result
				+ ((soortWaar == null) ? 0 : soortWaar.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterVwa other = (MonsterVwa) obj;
		if (analysemethode == null) {
			if (other.analysemethode != null)
				return false;
		} else if (!analysemethode.equals(other.analysemethode))
			return false;
		if (gemeenteMonstername == null) {
			if (other.gemeenteMonstername != null)
				return false;
		} else if (!gemeenteMonstername.equals(other.gemeenteMonstername))
			return false;
		if (herkomst == null) {
			if (other.herkomst != null)
				return false;
		} else if (!herkomst.equals(other.herkomst))
			return false;
		if (idMonster == null) {
			if (other.idMonster != null)
				return false;
		} else if (!idMonster.equals(other.idMonster))
			return false;
		if (registratienummer == null) {
			if (other.registratienummer != null)
				return false;
		} else if (!registratienummer.equals(other.registratienummer))
			return false;
		if (soortWaar == null) {
			if (other.soortWaar != null)
				return false;
		} else if (!soortWaar.equals(other.soortWaar))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterVwa [analysemethode=" + analysemethode
				+ ", gemeenteMonstername=" + gemeenteMonstername
				+ ", herkomst=" + herkomst + ", idMonster=" + idMonster
				+ ", registratienummer=" + registratienummer + ", soortWaar="
				+ soortWaar + "]";
	}

}

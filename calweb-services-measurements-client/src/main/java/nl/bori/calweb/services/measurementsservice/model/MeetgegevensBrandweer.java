package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "meetgegevens_brandweer")
public class MeetgegevensBrandweer implements java.io.Serializable {

	private static final long serialVersionUID = -754191026544507390L;

	@Id
	@Column(name = "id_meetgegevens", unique = true, nullable = false)
	private int idMeetgegevens;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_meetgegevens", unique = true, nullable = false, insertable = false, updatable = false)
	private Meetgegevens meetgegevens;

	@Column(name = "beschrijving", nullable = false)
	private String beschrijving;

	public MeetgegevensBrandweer() {
	}

	public int getIdMeetgegevens() {
		return this.idMeetgegevens;
	}

	public void setIdMeetgegevens(int idMeetgegevens) {
		this.idMeetgegevens = idMeetgegevens;
	}

	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	public String getBeschrijving() {
		return this.beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((beschrijving == null) ? 0 : beschrijving.hashCode());
		result = prime * result + idMeetgegevens;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeetgegevensBrandweer other = (MeetgegevensBrandweer) obj;
		if (beschrijving == null) {
			if (other.beschrijving != null)
				return false;
		} else if (!beschrijving.equals(other.beschrijving))
			return false;
		if (idMeetgegevens != other.idMeetgegevens)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeetgegevensBrandweer [beschrijving=" + beschrijving
				+ ", idMeetgegevens=" + idMeetgegevens + "]";
	}

}

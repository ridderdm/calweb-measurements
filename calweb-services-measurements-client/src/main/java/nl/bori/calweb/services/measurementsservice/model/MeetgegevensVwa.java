package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "meetgegevens_vwa")
public class MeetgegevensVwa implements java.io.Serializable {

	private static final long serialVersionUID = 7770101378386844506L;

	@Id
	@Column(name = "id_meetgegevens", unique = true, nullable = false)
	private int idMeetgegevens;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_meetgegevens", unique = true, nullable = false, insertable = false, updatable = false)
	private Meetgegevens meetgegevens;

	@Column(name = "contactpersoon", nullable = false, length = 100)
	private String contactpersoon;

	@Column(name = "telefoon", nullable = false, length = 15)
	private String telefoon;

	@Column(name = "codering", nullable = false, length = 25)
	private String codering;

	@Column(name = "beschrijving", nullable = false)
	private String beschrijving;

	public MeetgegevensVwa() {
	}

	public int getIdMeetgegevens() {
		return this.idMeetgegevens;
	}

	public void setIdMeetgegevens(int idMeetgegevens) {
		this.idMeetgegevens = idMeetgegevens;
	}

	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	public String getContactpersoon() {
		return this.contactpersoon;
	}

	public void setContactpersoon(String contactpersoon) {
		this.contactpersoon = contactpersoon;
	}

	public String getTelefoon() {
		return this.telefoon;
	}

	public void setTelefoon(String telefoon) {
		this.telefoon = telefoon;
	}

	public String getCodering() {
		return this.codering;
	}

	public void setCodering(String codering) {
		this.codering = codering;
	}

	public String getBeschrijving() {
		return this.beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((beschrijving == null) ? 0 : beschrijving.hashCode());
		result = prime * result
				+ ((codering == null) ? 0 : codering.hashCode());
		result = prime * result
				+ ((contactpersoon == null) ? 0 : contactpersoon.hashCode());
		result = prime * result + idMeetgegevens;
		result = prime * result
				+ ((telefoon == null) ? 0 : telefoon.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeetgegevensVwa other = (MeetgegevensVwa) obj;
		if (beschrijving == null) {
			if (other.beschrijving != null)
				return false;
		} else if (!beschrijving.equals(other.beschrijving))
			return false;
		if (codering == null) {
			if (other.codering != null)
				return false;
		} else if (!codering.equals(other.codering))
			return false;
		if (contactpersoon == null) {
			if (other.contactpersoon != null)
				return false;
		} else if (!contactpersoon.equals(other.contactpersoon))
			return false;
		if (idMeetgegevens != other.idMeetgegevens)
			return false;
		if (telefoon == null) {
			if (other.telefoon != null)
				return false;
		} else if (!telefoon.equals(other.telefoon))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeetgegevensVwa [beschrijving=" + beschrijving + ", codering="
				+ codering + ", contactpersoon=" + contactpersoon
				+ ", idMeetgegevens=" + idMeetgegevens + ", telefoon="
				+ telefoon + "]";
	}

}

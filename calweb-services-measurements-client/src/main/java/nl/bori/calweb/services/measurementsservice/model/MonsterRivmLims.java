package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "monster_rivm_lims")
public class MonsterRivmLims implements java.io.Serializable {

	private static final long serialVersionUID = 4020893826453131272L;

	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private int idMonster;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	private Monster monster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_eenheid_quantity", nullable = false)
	private Eenheid eenheid;

	@Column(name = "quantity", nullable = false, precision = 12, scale = 0)
	private float quantity;

	@Column(name = "correctionFactor", nullable = false, precision = 12, scale = 0)
	private float correctionFactor;

	@Column(name = "sample_template")
	private String sampleTemplate;

	@Column(name = "locatie")
	private String locatie;

	@Column(name = "plaats")
	private String plaats;

	@Column(name = "detectornaam")
	private String detectornaam;

	public MonsterRivmLims() {
	}

	public int getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(int idMonster) {
		this.idMonster = idMonster;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public Eenheid getEenheid() {
		return this.eenheid;
	}

	public void setEenheid(Eenheid eenheid) {
		this.eenheid = eenheid;
	}

	public float getQuantity() {
		return this.quantity;
	}

	public void setQuantity(float quantity) {
		this.quantity = quantity;
	}

	public float getCorrectionFactor() {
		return this.correctionFactor;
	}

	public void setCorrectionFactor(float correctionFactor) {
		this.correctionFactor = correctionFactor;
	}

	public String getSampleTemplate() {
		return this.sampleTemplate;
	}

	public void setSampleTemplate(String sampleTemplate) {
		this.sampleTemplate = sampleTemplate;
	}

	public String getLocatie() {
		return this.locatie;
	}

	public void setLocatie(String locatie) {
		this.locatie = locatie;
	}

	public String getPlaats() {
		return this.plaats;
	}

	public void setPlaats(String plaats) {
		this.plaats = plaats;
	}

	public String getDetectornaam() {
		return this.detectornaam;
	}

	public void setDetectornaam(String detectornaam) {
		this.detectornaam = detectornaam;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(correctionFactor);
		result = prime * result
				+ ((detectornaam == null) ? 0 : detectornaam.hashCode());
		result = prime * result + ((eenheid == null) ? 0 : eenheid.hashCode());
		result = prime * result + idMonster;
		result = prime * result + ((locatie == null) ? 0 : locatie.hashCode());
		result = prime * result + ((plaats == null) ? 0 : plaats.hashCode());
		result = prime * result + Float.floatToIntBits(quantity);
		result = prime * result
				+ ((sampleTemplate == null) ? 0 : sampleTemplate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterRivmLims other = (MonsterRivmLims) obj;
		if (Float.floatToIntBits(correctionFactor) != Float
				.floatToIntBits(other.correctionFactor))
			return false;
		if (detectornaam == null) {
			if (other.detectornaam != null)
				return false;
		} else if (!detectornaam.equals(other.detectornaam))
			return false;
		if (eenheid == null) {
			if (other.eenheid != null)
				return false;
		} else if (!eenheid.equals(other.eenheid))
			return false;
		if (idMonster != other.idMonster)
			return false;
		if (locatie == null) {
			if (other.locatie != null)
				return false;
		} else if (!locatie.equals(other.locatie))
			return false;
		if (plaats == null) {
			if (other.plaats != null)
				return false;
		} else if (!plaats.equals(other.plaats))
			return false;
		if (Float.floatToIntBits(quantity) != Float
				.floatToIntBits(other.quantity))
			return false;
		if (sampleTemplate == null) {
			if (other.sampleTemplate != null)
				return false;
		} else if (!sampleTemplate.equals(other.sampleTemplate))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterRivmLims [correctionFactor=" + correctionFactor
				+ ", detectornaam=" + detectornaam + ", eenheid=" + eenheid
				+ ", idMonster=" + idMonster + ", locatie=" + locatie
				+ ", plaats=" + plaats + ", quantity=" + quantity
				+ ", sampleTemplate=" + sampleTemplate + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "eenheid", uniqueConstraints = @UniqueConstraint(columnNames = "eenheid"))
public class Eenheid implements java.io.Serializable {

	private static final long serialVersionUID = 2045383889550887300L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "eenheid", unique = true, nullable = false, length = 15)
	private String eenheid;

	@Column(name = "si_eenheid", length = 15)
	private String siEenheid;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "eenheidByIdEenheidOnzekerheid")
	private List<BepalingRivmLims> bepalingRivmLimsesForIdEenheidOnzekerheid = new ArrayList<BepalingRivmLims>(
			0);

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "eenheid")
	private List<Bepaling> bepalings = new ArrayList<Bepaling>(0);

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "eenheidByIdEenheidDetectiegrens")
	private List<BepalingRivmLims> bepalingRivmLimsesForIdEenheidDetectiegrens = new ArrayList<BepalingRivmLims>(
			0);

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "eenheid")
	private List<MonsterRivmLims> monsterRivmLimses = new ArrayList<MonsterRivmLims>(
			0);

	public Eenheid() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEenheid() {
		return this.eenheid;
	}

	public void setEenheid(String eenheid) {
		this.eenheid = eenheid;
	}

	public String getSiEenheid() {
		return this.siEenheid;
	}

	public void setSiEenheid(String siEenheid) {
		this.siEenheid = siEenheid;
	}

	public List<BepalingRivmLims> getBepalingRivmLimsesForIdEenheidOnzekerheid() {
		return this.bepalingRivmLimsesForIdEenheidOnzekerheid;
	}

	public void setBepalingRivmLimsesForIdEenheidOnzekerheid(
			List<BepalingRivmLims> bepalingRivmLimsesForIdEenheidOnzekerheid) {
		this.bepalingRivmLimsesForIdEenheidOnzekerheid = bepalingRivmLimsesForIdEenheidOnzekerheid;
	}

	public List<Bepaling> getBepalings() {
		return this.bepalings;
	}

	public void setBepalings(List<Bepaling> bepalings) {
		this.bepalings = bepalings;
	}

	public List<BepalingRivmLims> getBepalingRivmLimsesForIdEenheidDetectiegrens() {
		return this.bepalingRivmLimsesForIdEenheidDetectiegrens;
	}

	public void setBepalingRivmLimsesForIdEenheidDetectiegrens(
			List<BepalingRivmLims> bepalingRivmLimsesForIdEenheidDetectiegrens) {
		this.bepalingRivmLimsesForIdEenheidDetectiegrens = bepalingRivmLimsesForIdEenheidDetectiegrens;
	}

	public List<MonsterRivmLims> getMonsterRivmLimses() {
		return this.monsterRivmLimses;
	}

	public void setMonsterRivmLimses(List<MonsterRivmLims> monsterRivmLimses) {
		this.monsterRivmLimses = monsterRivmLimses;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eenheid == null) ? 0 : eenheid.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((siEenheid == null) ? 0 : siEenheid.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Eenheid other = (Eenheid) obj;
		if (eenheid == null) {
			if (other.eenheid != null)
				return false;
		} else if (!eenheid.equals(other.eenheid))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (siEenheid == null) {
			if (other.siEenheid != null)
				return false;
		} else if (!siEenheid.equals(other.siEenheid))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Eenheid [eenheid=" + eenheid + ", id=" + id + ", siEenheid="
				+ siEenheid + "]";
	}

}

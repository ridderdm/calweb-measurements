package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "meetgegevens_status")
public class MeetgegevensStatus implements java.io.Serializable {

	private static final long serialVersionUID = -3853874497139691147L;

	private Integer id;

	private Meetgegevens meetgegevens;

	private String status;

	private Date datum;

	private String verantwoordelijke;

	private String reden;

	public MeetgegevensStatus() {
	}

	public MeetgegevensStatus(Meetgegevens meetgegevens, String status,
			Date datum, String verantwoordelijke, String reden) {
		this.meetgegevens = meetgegevens;
		this.status = status;
		this.datum = datum;
		this.verantwoordelijke = verantwoordelijke;
		this.reden = reden;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_meetgegevens", nullable = false)
	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	@Column(name = "status", nullable = false, length = 10)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datum", nullable = false, length = 19)
	public Date getDatum() {
		return this.datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	@Column(name = "verantwoordelijke", nullable = false)
	public String getVerantwoordelijke() {
		return this.verantwoordelijke;
	}

	public void setVerantwoordelijke(String verantwoordelijke) {
		this.verantwoordelijke = verantwoordelijke;
	}

	@Column(name = "reden", nullable = false)
	public String getReden() {
		return this.reden;
	}

	public void setReden(String reden) {
		this.reden = reden;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((datum == null) ? 0 : datum.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((reden == null) ? 0 : reden.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime
				* result
				+ ((verantwoordelijke == null) ? 0 : verantwoordelijke
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeetgegevensStatus other = (MeetgegevensStatus) obj;
		if (datum == null) {
			if (other.datum != null)
				return false;
		} else if (!datum.equals(other.datum))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (reden == null) {
			if (other.reden != null)
				return false;
		} else if (!reden.equals(other.reden))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (verantwoordelijke == null) {
			if (other.verantwoordelijke != null)
				return false;
		} else if (!verantwoordelijke.equals(other.verantwoordelijke))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeetgegevensStatus [datum=" + datum + ", id=" + id + ", reden="
				+ reden + ", status=" + status + ", verantwoordelijke="
				+ verantwoordelijke + "]";
	}

}

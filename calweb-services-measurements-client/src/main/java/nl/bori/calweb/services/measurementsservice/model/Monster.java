package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "monster")
public class Monster implements java.io.Serializable {

	private static final long serialVersionUID = 7692476438779828357L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_locatie")
	private Locatie locatie;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_meetgegevens", nullable = false)
	private Meetgegevens meetgegevens;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "monster_begin", nullable = false, length = 19)
	private Date monsterBegin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "monster_eind", nullable = false, length = 19)
	private Date monsterEind;
	
	@ManyToOne
	@JoinColumn(name = "id_substance", nullable = false)
	private Substance substance;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "monster")
	private List<MonsterStatus> monsterStatuses = new ArrayList<MonsterStatus>(
			0);

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "monster")
	private MonsterRizaExcel monsterRizaExcel;
	
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "monster")
	private MonsterRikilt monsterRikilt;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "monster")
	private MonsterBrandweer monsterBrandweer;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "monster")
	private MonsterVwa monsterVwa;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "monster")
	private MonsterRizaXml monsterRizaXml;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "monster")
	private MonsterRivmLims monsterRivmLims;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "monster")
	private MonsterRivm monsterRivm;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "monster")
	private List<Bepaling> bepalings = new ArrayList<Bepaling>(0);
	
	public Monster() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Locatie getLocatie() {
		return this.locatie;
	}

	public void setLocatie(Locatie locatie) {
		this.locatie = locatie;
	}

	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	public Date getMonsterBegin() {
		return this.monsterBegin;
	}

	public void setMonsterBegin(Date monsterBegin) {
		this.monsterBegin = monsterBegin;
	}

	public Date getMonsterEind() {
		return this.monsterEind;
	}

	public void setMonsterEind(Date monsterEind) {
		this.monsterEind = monsterEind;
	}

	/**
	 * @param substance the substance to set
	 */
	public void setSubstance(Substance substance) {
		this.substance = substance;
	}

	/**
	 * @return the substance
	 */
	public Substance getSubstance() {
		return this.substance;
	}
	
	/**
	 * @return the monsterRizaExcel
	 */
	public MonsterRizaExcel getMonsterRizaExcel() {
		return monsterRizaExcel;
	}

	/**
	 * @param monsterRizaExcel the monsterRizaExcel to set
	 */
	public void setMonsterRizaExcel(MonsterRizaExcel monsterRizaExcel) {
		this.monsterRizaExcel = monsterRizaExcel;
	}

	public List<MonsterStatus> getMonsterStatuses() {
		return this.monsterStatuses;
	}

	public void setMonsterStatuses(List<MonsterStatus> monsterStatuses) {
		this.monsterStatuses = monsterStatuses;
	}

	public MonsterRikilt getMonsterRikilt() {
		return this.monsterRikilt;
	}

	public void setMonsterRikilt(MonsterRikilt monsterRikilt) {
		this.monsterRikilt = monsterRikilt;
	}

	public MonsterBrandweer getMonsterBrandweer() {
		return this.monsterBrandweer;
	}

	public void setMonsterBrandweer(MonsterBrandweer monsterBrandweer) {
		this.monsterBrandweer = monsterBrandweer;
	}

	public List<Bepaling> getBepalings() {
		return this.bepalings;
	}

	public void setBepalings(List<Bepaling> bepalings) {
		this.bepalings = bepalings;
	}

	public MonsterVwa getMonsterVwa() {
		return this.monsterVwa;
	}

	public void setMonsterVwa(MonsterVwa monsterVwa) {
		this.monsterVwa = monsterVwa;
	}

	public MonsterRizaXml getMonsterRizaXml() {
		return this.monsterRizaXml;
	}

	public void setMonsterRizaXml(MonsterRizaXml monsterRizaXml) {
		this.monsterRizaXml = monsterRizaXml;
	}

	public MonsterRivmLims getMonsterRivmLims() {
		return this.monsterRivmLims;
	}

	public void setMonsterRivmLims(MonsterRivmLims monsterRivmLims) {
		this.monsterRivmLims = monsterRivmLims;
	}

	public MonsterRivm getMonsterRivm() {
		return this.monsterRivm;
	}

	public void setMonsterRivm(MonsterRivm monsterRivm) {
		this.monsterRivm = monsterRivm;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bepalings == null) ? 0 : bepalings.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((locatie == null) ? 0 : locatie.hashCode());
		result = prime * result
				+ ((monsterBegin == null) ? 0 : monsterBegin.hashCode());
		result = prime
				* result
				+ ((monsterBrandweer == null) ? 0 : monsterBrandweer.hashCode());
		result = prime * result
				+ ((monsterEind == null) ? 0 : monsterEind.hashCode());
		result = prime * result
				+ ((monsterRikilt == null) ? 0 : monsterRikilt.hashCode());
		result = prime * result
				+ ((monsterRivm == null) ? 0 : monsterRivm.hashCode());
		result = prime * result
				+ ((monsterRivmLims == null) ? 0 : monsterRivmLims.hashCode());
		result = prime
				* result
				+ ((monsterRizaExcel == null) ? 0 : monsterRizaExcel.hashCode());
		result = prime * result
				+ ((monsterRizaXml == null) ? 0 : monsterRizaXml.hashCode());
		result = prime * result
				+ ((monsterStatuses == null) ? 0 : monsterStatuses.hashCode());
		result = prime * result
				+ ((monsterVwa == null) ? 0 : monsterVwa.hashCode());
		result = prime * result
				+ ((substance == null) ? 0 : substance.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monster other = (Monster) obj;
		if (bepalings == null) {
			if (other.bepalings != null)
				return false;
		} else if (!bepalings.equals(other.bepalings))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (locatie == null) {
			if (other.locatie != null)
				return false;
		} else if (!locatie.equals(other.locatie))
			return false;
		if (monsterBegin == null) {
			if (other.monsterBegin != null)
				return false;
		} else if (!monsterBegin.equals(other.monsterBegin))
			return false;
		if (monsterBrandweer == null) {
			if (other.monsterBrandweer != null)
				return false;
		} else if (!monsterBrandweer.equals(other.monsterBrandweer))
			return false;
		if (monsterEind == null) {
			if (other.monsterEind != null)
				return false;
		} else if (!monsterEind.equals(other.monsterEind))
			return false;
		if (monsterRikilt == null) {
			if (other.monsterRikilt != null)
				return false;
		} else if (!monsterRikilt.equals(other.monsterRikilt))
			return false;
		if (monsterRivm == null) {
			if (other.monsterRivm != null)
				return false;
		} else if (!monsterRivm.equals(other.monsterRivm))
			return false;
		if (monsterRivmLims == null) {
			if (other.monsterRivmLims != null)
				return false;
		} else if (!monsterRivmLims.equals(other.monsterRivmLims))
			return false;
		if (monsterRizaExcel == null) {
			if (other.monsterRizaExcel != null)
				return false;
		} else if (!monsterRizaExcel.equals(other.monsterRizaExcel))
			return false;
		if (monsterRizaXml == null) {
			if (other.monsterRizaXml != null)
				return false;
		} else if (!monsterRizaXml.equals(other.monsterRizaXml))
			return false;
		if (monsterStatuses == null) {
			if (other.monsterStatuses != null)
				return false;
		} else if (!monsterStatuses.equals(other.monsterStatuses))
			return false;
		if (monsterVwa == null) {
			if (other.monsterVwa != null)
				return false;
		} else if (!monsterVwa.equals(other.monsterVwa))
			return false;
		if (substance == null) {
			if (other.substance != null)
				return false;
		} else if (!substance.equals(other.substance))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Monster [bepalings=" + bepalings + ", id=" + id + ", locatie="
				+ locatie + ", monsterBegin=" + monsterBegin
				+ ", monsterBrandweer=" + monsterBrandweer + ", monsterEind="
				+ monsterEind + ", monsterRikilt=" + monsterRikilt
				+ ", monsterRivm=" + monsterRivm + ", monsterRivmLims="
				+ monsterRivmLims + ", monsterRizaExcel=" + monsterRizaExcel
				+ ", monsterRizaXml=" + monsterRizaXml + ", monsterStatuses="
				+ monsterStatuses + ", monsterVwa=" + monsterVwa
				+ ", substance=" + substance + "]";
	}

}

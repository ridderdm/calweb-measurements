package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bepaling_rivm")
public class BepalingRivm implements java.io.Serializable {

	private static final long serialVersionUID = 3324735673698303101L;

	@Id
	@Column(name = "id_bepaling", unique = true, nullable = false)
	private int idBepaling;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bepaling", unique = true, nullable = false, insertable = false, updatable = false)
	private Bepaling bepaling;

	@Column(name = "type_bepaling", nullable = false)
	private String typeBepaling;

	@Column(name = "onzekerheid", nullable = false, precision = 12, scale = 0)
	private float onzekerheid;

	public BepalingRivm() {
	}


	public int getIdBepaling() {
		return this.idBepaling;
	}

	public void setIdBepaling(int idBepaling) {
		this.idBepaling = idBepaling;
	}

	public Bepaling getBepaling() {
		return this.bepaling;
	}

	public void setBepaling(Bepaling bepaling) {
		this.bepaling = bepaling;
	}

	public String getTypeBepaling() {
		return this.typeBepaling;
	}

	public void setTypeBepaling(String typeBepaling) {
		this.typeBepaling = typeBepaling;
	}

	public float getOnzekerheid() {
		return this.onzekerheid;
	}

	public void setOnzekerheid(float onzekerheid) {
		this.onzekerheid = onzekerheid;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idBepaling;
		result = prime * result + Float.floatToIntBits(onzekerheid);
		result = prime * result
				+ ((typeBepaling == null) ? 0 : typeBepaling.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BepalingRivm other = (BepalingRivm) obj;
		if (idBepaling != other.idBepaling)
			return false;
		if (Float.floatToIntBits(onzekerheid) != Float
				.floatToIntBits(other.onzekerheid))
			return false;
		if (typeBepaling == null) {
			if (other.typeBepaling != null)
				return false;
		} else if (!typeBepaling.equals(other.typeBepaling))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BepalingRivm [idBepaling=" + idBepaling + ", onzekerheid="
				+ onzekerheid + ", typeBepaling=" + typeBepaling + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SubdeelcompartimentInfoId implements java.io.Serializable {

	private static final long serialVersionUID = 7100033565660278041L;

	private int compnr;

	private int deelcompnr;

	private int nummer;

	public SubdeelcompartimentInfoId() {
	}

	public SubdeelcompartimentInfoId(int compnr, int deelcompnr, int nummer) {
		this.compnr = compnr;
		this.deelcompnr = deelcompnr;
		this.nummer = nummer;
	}

	@Column(name = "compnr", nullable = false)
	public int getCompnr() {
		return this.compnr;
	}

	public void setCompnr(int compnr) {
		this.compnr = compnr;
	}

	@Column(name = "deelcompnr", nullable = false)
	public int getDeelcompnr() {
		return this.deelcompnr;
	}

	public void setDeelcompnr(int deelcompnr) {
		this.deelcompnr = deelcompnr;
	}

	@Column(name = "nummer", nullable = false)
	public int getNummer() {
		return this.nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SubdeelcompartimentInfoId))
			return false;
		SubdeelcompartimentInfoId castOther = (SubdeelcompartimentInfoId) other;

		return (this.getCompnr() == castOther.getCompnr())
				&& (this.getDeelcompnr() == castOther.getDeelcompnr())
				&& (this.getNummer() == castOther.getNummer());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getCompnr();
		result = 37 * result + this.getDeelcompnr();
		result = 37 * result + this.getNummer();
		return result;
	}

}

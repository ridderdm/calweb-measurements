package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "monster_rikilt")
public class MonsterRikilt implements java.io.Serializable {

	private static final long serialVersionUID = -911687830478545636L;

	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private int idMonster;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	private Monster monster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_compartiment", nullable = false)
	private Compartiment compartiment;

	@Column(name = "LMRV_id", nullable = false, length = 15)
	private String lmrvId;

	@Column(name = "eigenaar", nullable = false, length = 7)
	private String eigenaar;

	@Column(name = "import", nullable = false, length = 5)
	private String import_;

	@Column(name = "vwa_registratie_nummer", nullable = false, length = 25)
	private String vwaRegistratieNummer;

	@Column(name = "opmerking_onderzoek", nullable = false)
	private String opmerkingOnderzoek;

	@Column(name = "bemonsteringsmethode", nullable = false, length = 16)
	private String bemonsteringsmethode;

	@Column(name = "landvanherkomst", nullable = false)
	private String landvanherkomst;

	@Column(name = "plaats_omschrijving", nullable = false)
	private String plaatsOmschrijving;

	public MonsterRikilt() {
	}

	public int getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(int idMonster) {
		this.idMonster = idMonster;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public Compartiment getCompartiment() {
		return this.compartiment;
	}

	public void setCompartiment(Compartiment compartiment) {
		this.compartiment = compartiment;
	}

	public String getLmrvId() {
		return this.lmrvId;
	}

	public void setLmrvId(String lmrvId) {
		this.lmrvId = lmrvId;
	}

	public String getEigenaar() {
		return this.eigenaar;
	}

	public void setEigenaar(String eigenaar) {
		this.eigenaar = eigenaar;
	}

	public String getImport_() {
		return this.import_;
	}

	public void setImport_(String import_) {
		this.import_ = import_;
	}

	public String getVwaRegistratieNummer() {
		return this.vwaRegistratieNummer;
	}

	public void setVwaRegistratieNummer(String vwaRegistratieNummer) {
		this.vwaRegistratieNummer = vwaRegistratieNummer;
	}

	public String getOpmerkingOnderzoek() {
		return this.opmerkingOnderzoek;
	}

	public void setOpmerkingOnderzoek(String opmerkingOnderzoek) {
		this.opmerkingOnderzoek = opmerkingOnderzoek;
	}

	public String getBemonsteringsmethode() {
		return this.bemonsteringsmethode;
	}

	public void setBemonsteringsmethode(String bemonsteringsmethode) {
		this.bemonsteringsmethode = bemonsteringsmethode;
	}

	public String getLandvanherkomst() {
		return this.landvanherkomst;
	}

	public void setLandvanherkomst(String landvanherkomst) {
		this.landvanherkomst = landvanherkomst;
	}

	public String getPlaatsOmschrijving() {
		return this.plaatsOmschrijving;
	}

	public void setPlaatsOmschrijving(String plaatsOmschrijving) {
		this.plaatsOmschrijving = plaatsOmschrijving;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((bemonsteringsmethode == null) ? 0 : bemonsteringsmethode
						.hashCode());
		result = prime * result
				+ ((compartiment == null) ? 0 : compartiment.hashCode());
		result = prime * result
				+ ((eigenaar == null) ? 0 : eigenaar.hashCode());
		result = prime * result + idMonster;
		result = prime * result + ((import_ == null) ? 0 : import_.hashCode());
		result = prime * result
				+ ((landvanherkomst == null) ? 0 : landvanherkomst.hashCode());
		result = prime * result + ((lmrvId == null) ? 0 : lmrvId.hashCode());
		result = prime
				* result
				+ ((opmerkingOnderzoek == null) ? 0 : opmerkingOnderzoek
						.hashCode());
		result = prime
				* result
				+ ((plaatsOmschrijving == null) ? 0 : plaatsOmschrijving
						.hashCode());
		result = prime
				* result
				+ ((vwaRegistratieNummer == null) ? 0 : vwaRegistratieNummer
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterRikilt other = (MonsterRikilt) obj;
		if (bemonsteringsmethode == null) {
			if (other.bemonsteringsmethode != null)
				return false;
		} else if (!bemonsteringsmethode.equals(other.bemonsteringsmethode))
			return false;
		if (compartiment == null) {
			if (other.compartiment != null)
				return false;
		} else if (!compartiment.equals(other.compartiment))
			return false;
		if (eigenaar == null) {
			if (other.eigenaar != null)
				return false;
		} else if (!eigenaar.equals(other.eigenaar))
			return false;
		if (idMonster != other.idMonster)
			return false;
		if (import_ == null) {
			if (other.import_ != null)
				return false;
		} else if (!import_.equals(other.import_))
			return false;
		if (landvanherkomst == null) {
			if (other.landvanherkomst != null)
				return false;
		} else if (!landvanherkomst.equals(other.landvanherkomst))
			return false;
		if (lmrvId == null) {
			if (other.lmrvId != null)
				return false;
		} else if (!lmrvId.equals(other.lmrvId))
			return false;
		if (opmerkingOnderzoek == null) {
			if (other.opmerkingOnderzoek != null)
				return false;
		} else if (!opmerkingOnderzoek.equals(other.opmerkingOnderzoek))
			return false;
		if (plaatsOmschrijving == null) {
			if (other.plaatsOmschrijving != null)
				return false;
		} else if (!plaatsOmschrijving.equals(other.plaatsOmschrijving))
			return false;
		if (vwaRegistratieNummer == null) {
			if (other.vwaRegistratieNummer != null)
				return false;
		} else if (!vwaRegistratieNummer.equals(other.vwaRegistratieNummer))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterRikilt [bemonsteringsmethode=" + bemonsteringsmethode
				+ ", compartiment=" + compartiment + ", eigenaar=" + eigenaar
				+ ", idMonster=" + idMonster + ", import_=" + import_
				+ ", landvanherkomst=" + landvanherkomst + ", lmrvId=" + lmrvId
				+ ", opmerkingOnderzoek=" + opmerkingOnderzoek
				+ ", plaatsOmschrijving=" + plaatsOmschrijving
				+ ", vwaRegistratieNummer=" + vwaRegistratieNummer + "]";
	}

}

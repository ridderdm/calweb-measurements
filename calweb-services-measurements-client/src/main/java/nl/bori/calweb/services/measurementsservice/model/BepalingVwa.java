package nl.bori.calweb.services.measurementsservice.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bepaling_vwa")
public class BepalingVwa implements java.io.Serializable {

	private static final long serialVersionUID = 8999957482004004294L;

	@Id
	@Column(name = "id_bepaling", unique = true, nullable = false)
	private int idBepaling;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bepaling", unique = true, nullable = false, insertable = false, updatable = false)
	private Bepaling bepaling;

	@Column(name = "onzekerheid", nullable = false, precision = 12, scale = 0)
	private float onzekerheid;

	@Column(name = "rapportagegrens", nullable = false, precision = 12, scale = 0)
	private float rapportagegrens;

	@Column(name = "opmerkingen", nullable = false)
	private String opmerkingen;

	public BepalingVwa() {
	}

	public int getIdBepaling() {
		return this.idBepaling;
	}

	public void setIdBepaling(int idBepaling) {
		this.idBepaling = idBepaling;
	}

	public Bepaling getBepaling() {
		return this.bepaling;
	}

	public void setBepaling(Bepaling bepaling) {
		this.bepaling = bepaling;
	}

	public float getOnzekerheid() {
		return this.onzekerheid;
	}

	public void setOnzekerheid(float onzekerheid) {
		this.onzekerheid = onzekerheid;
	}

	public float getRapportagegrens() {
		return this.rapportagegrens;
	}

	public void setRapportagegrens(float rapportagegrens) {
		this.rapportagegrens = rapportagegrens;
	}

	public String getOpmerkingen() {
		return this.opmerkingen;
	}

	public void setOpmerkingen(String opmerkingen) {
		this.opmerkingen = opmerkingen;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idBepaling;
		result = prime * result + Float.floatToIntBits(onzekerheid);
		result = prime * result
				+ ((opmerkingen == null) ? 0 : opmerkingen.hashCode());
		result = prime * result + Float.floatToIntBits(rapportagegrens);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BepalingVwa other = (BepalingVwa) obj;
		if (idBepaling != other.idBepaling)
			return false;
		if (Float.floatToIntBits(onzekerheid) != Float
				.floatToIntBits(other.onzekerheid))
			return false;
		if (opmerkingen == null) {
			if (other.opmerkingen != null)
				return false;
		} else if (!opmerkingen.equals(other.opmerkingen))
			return false;
		if (Float.floatToIntBits(rapportagegrens) != Float
				.floatToIntBits(other.rapportagegrens))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BepalingVwa [idBepaling=" + idBepaling + ", onzekerheid="
				+ onzekerheid + ", opmerkingen=" + opmerkingen
				+ ", rapportagegrens=" + rapportagegrens + "]";
	}

}

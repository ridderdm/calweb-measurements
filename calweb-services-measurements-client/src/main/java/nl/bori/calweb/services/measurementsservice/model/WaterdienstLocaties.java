package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "waterdienstLocaties")
public class WaterdienstLocaties implements java.io.Serializable {

	private static final long serialVersionUID = 1483849490327657743L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private int id;

	@Column(name = "beheerder", nullable = false, length = 200)
	private String beheerder;

	@Column(name = "naam", nullable = false, length = 200)
	private String naam;

	@Column(name = "xcoordinaat", nullable = false)
	private int xcoordinaat;

	@Column(name = "ycoordinaat", nullable = false)
	private int ycoordinaat;

	@Column(name = "beschrijving", nullable = false, length = 200)
	private String beschrijving;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "waterdienstLocaties")
	private List<MonsterRizaExcel> monsterRizaExcels = new ArrayList<MonsterRizaExcel>(
			0);

	public WaterdienstLocaties() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBeheerder() {
		return this.beheerder;
	}

	public void setBeheerder(String beheerder) {
		this.beheerder = beheerder;
	}

	public String getNaam() {
		return this.naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public int getXcoordinaat() {
		return this.xcoordinaat;
	}

	public void setXcoordinaat(int xcoordinaat) {
		this.xcoordinaat = xcoordinaat;
	}

	public int getYcoordinaat() {
		return this.ycoordinaat;
	}

	public void setYcoordinaat(int ycoordinaat) {
		this.ycoordinaat = ycoordinaat;
	}

	public String getBeschrijving() {
		return this.beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}

	public List<MonsterRizaExcel> getMonsterRizaExcels() {
		return this.monsterRizaExcels;
	}

	public void setMonsterRizaExcels(List<MonsterRizaExcel> monsterRizaExcels) {
		this.monsterRizaExcels = monsterRizaExcels;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((beheerder == null) ? 0 : beheerder.hashCode());
		result = prime * result
				+ ((beschrijving == null) ? 0 : beschrijving.hashCode());
		result = prime * result + id;
		result = prime * result + ((naam == null) ? 0 : naam.hashCode());
		result = prime * result + xcoordinaat;
		result = prime * result + ycoordinaat;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WaterdienstLocaties other = (WaterdienstLocaties) obj;
		if (beheerder == null) {
			if (other.beheerder != null)
				return false;
		} else if (!beheerder.equals(other.beheerder))
			return false;
		if (beschrijving == null) {
			if (other.beschrijving != null)
				return false;
		} else if (!beschrijving.equals(other.beschrijving))
			return false;
		if (id != other.id)
			return false;
		if (naam == null) {
			if (other.naam != null)
				return false;
		} else if (!naam.equals(other.naam))
			return false;
		if (xcoordinaat != other.xcoordinaat)
			return false;
		if (ycoordinaat != other.ycoordinaat)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WaterdienstLocaties [beheerder=" + beheerder
				+ ", beschrijving=" + beschrijving + ", id=" + id + ", naam="
				+ naam + ", xcoordinaat=" + xcoordinaat + ", ycoordinaat="
				+ ycoordinaat + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "subdeelcompartimentInfo")
public class SubdeelcompartimentInfo implements java.io.Serializable {

	private static final long serialVersionUID = 806537158062487168L;

	@EmbeddedId
	@AttributeOverrides( {
			@AttributeOverride(name = "compnr", column = @Column(name = "compnr", nullable = false)),
			@AttributeOverride(name = "deelcompnr", column = @Column(name = "deelcompnr", nullable = false)),
			@AttributeOverride(name = "nummer", column = @Column(name = "nummer", nullable = false)) })
	private SubdeelcompartimentInfoId id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "compnr", referencedColumnName = "compnr", nullable = false, insertable = false, updatable = false),
			@JoinColumn(name = "deelcompnr", referencedColumnName = "nummer", nullable = false, insertable = false, updatable = false) })
	private DeelcompartimentInfo deelcompartimentInfo;

	@Column(name = "naam", nullable = false, length = 200)
	private String naam;

	@Column(name = "afkorting", nullable = false, length = 50)
	private String afkorting;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "subdeelcompartimentInfo")
	private List<Compartiment> compartiments = new ArrayList<Compartiment>(0);

	public SubdeelcompartimentInfo() {
	}

	public SubdeelcompartimentInfoId getId() {
		return this.id;
	}

	public void setId(SubdeelcompartimentInfoId id) {
		this.id = id;
	}

	public DeelcompartimentInfo getDeelcompartimentInfo() {
		return this.deelcompartimentInfo;
	}

	public void setDeelcompartimentInfo(
			DeelcompartimentInfo deelcompartimentInfo) {
		this.deelcompartimentInfo = deelcompartimentInfo;
	}

	public String getNaam() {
		return this.naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getAfkorting() {
		return this.afkorting;
	}

	public void setAfkorting(String afkorting) {
		this.afkorting = afkorting;
	}

	public List<Compartiment> getCompartiments() {
		return this.compartiments;
	}

	public void setCompartiments(List<Compartiment> compartiments) {
		this.compartiments = compartiments;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((afkorting == null) ? 0 : afkorting.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((naam == null) ? 0 : naam.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubdeelcompartimentInfo other = (SubdeelcompartimentInfo) obj;
		if (afkorting == null) {
			if (other.afkorting != null)
				return false;
		} else if (!afkorting.equals(other.afkorting))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (naam == null) {
			if (other.naam != null)
				return false;
		} else if (!naam.equals(other.naam))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SubdeelcompartimentInfo [afkorting=" + afkorting + ", id=" + id
				+ ", naam=" + naam + "]";
	}

}

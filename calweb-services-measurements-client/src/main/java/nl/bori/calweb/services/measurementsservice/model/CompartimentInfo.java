package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "compartimentInfo")
public class CompartimentInfo implements java.io.Serializable {

	private static final long serialVersionUID = -8265624620403912164L;

	@Id
	@Column(name = "nummer", unique = true, nullable = false)
	private int nummer;

	@Column(name = "naam", nullable = false, length = 200)
	private String naam;

	@Column(name = "afkorting", nullable = false, length = 50)
	private String afkorting;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "compartimentInfo")
	private List<DeelcompartimentInfo> deelcompartimentInfos = new ArrayList<DeelcompartimentInfo>(
			0);

	public CompartimentInfo() {
	}

	public int getNummer() {
		return this.nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public String getNaam() {
		return this.naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getAfkorting() {
		return this.afkorting;
	}

	public void setAfkorting(String afkorting) {
		this.afkorting = afkorting;
	}

	public List<DeelcompartimentInfo> getDeelcompartimentInfos() {
		return this.deelcompartimentInfos;
	}

	public void setDeelcompartimentInfos(
			List<DeelcompartimentInfo> deelcompartimentInfos) {
		this.deelcompartimentInfos = deelcompartimentInfos;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((afkorting == null) ? 0 : afkorting.hashCode());
		result = prime
				* result
				+ ((deelcompartimentInfos == null) ? 0 : deelcompartimentInfos
						.hashCode());
		result = prime * result + ((naam == null) ? 0 : naam.hashCode());
		result = prime * result + nummer;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompartimentInfo other = (CompartimentInfo) obj;
		if (afkorting == null) {
			if (other.afkorting != null)
				return false;
		} else if (!afkorting.equals(other.afkorting))
			return false;
		if (deelcompartimentInfos == null) {
			if (other.deelcompartimentInfos != null)
				return false;
		} else if (!deelcompartimentInfos.equals(other.deelcompartimentInfos))
			return false;
		if (naam == null) {
			if (other.naam != null)
				return false;
		} else if (!naam.equals(other.naam))
			return false;
		if (nummer != other.nummer)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CompartimentInfo [afkorting=" + afkorting + ", naam=" + naam
				+ ", nummer=" + nummer + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "meetgegevens", uniqueConstraints = @UniqueConstraint(columnNames = "filestorageIdentifier"))
public class Meetgegevens implements java.io.Serializable {

	private static final long serialVersionUID = -8720621616142205344L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_afzender", nullable = false)
	private Afzender afzender;

	@Column(name = "id_calwebstatus", nullable = false)
	private int idCalwebstatus;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datum_ontvangst", nullable = false, length = 19)
	private Date datumOntvangst;

	@Column(name = "type", length = 13)
	private String type;

	@Column(name = "reden", nullable = false, length = 14)
	private String reden;

	@Column(name = "filestorageIdentifier", unique = true)
	private String filestorageIdentifier;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "meetgegevens")
	private MeetgegevensRivm meetgegevensRivm;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private Bestand bestand;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private MeetgegevensRivmLims meetgegevensRivmLims;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private MeetgegevensRikilt meetgegevensRikilt;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private List<MeetgegevensStatus> meetgegevensStatuses = new ArrayList<MeetgegevensStatus>(0);

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private List<Monster> monsters = new ArrayList<Monster>(0);

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private MeetgegevensRizaXml meetgegevensRizaXml;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private MeetgegevensRizaExcel meetgegevensRizaExcel;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private MeetgegevensBrandweer meetgegevensBrandweer;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "meetgegevens")
	private MeetgegevensVwa meetgegevensVwa;

	public Meetgegevens() {
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Afzender getAfzender() {
		return this.afzender;
	}

	public void setAfzender(Afzender afzender) {
		this.afzender = afzender;
	}

	public int getIdCalwebstatus() {
		return this.idCalwebstatus;
	}

	public void setIdCalwebstatus(int idCalwebstatus) {
		this.idCalwebstatus = idCalwebstatus;
	}

	public Date getDatumOntvangst() {
		return this.datumOntvangst;
	}

	public void setDatumOntvangst(Date datumOntvangst) {
		this.datumOntvangst = datumOntvangst;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReden() {
		return this.reden;
	}

	public void setReden(String reden) {
		this.reden = reden;
	}

	public String getFilestorageIdentifier() {
		return this.filestorageIdentifier;
	}

	public void setFilestorageIdentifier(String filestorageIdentifier) {
		this.filestorageIdentifier = filestorageIdentifier;
	}

	public Bestand getBestand() {
		return this.bestand;
	}

	public void setBestand(Bestand bestand) {
		this.bestand = bestand;
	}

	public List<MeetgegevensStatus> getMeetgegevensStatuses() {
		return this.meetgegevensStatuses;
	}

	public void setMeetgegevensStatuses(
			List<MeetgegevensStatus> meetgegevensStatuses) {
		this.meetgegevensStatuses = meetgegevensStatuses;
	}

	public List<Monster> getMonsters() {
		return this.monsters;
	}

	public void setMonsters(List<Monster> monsters) {
		this.monsters = monsters;
	}

	/**
	 * @return the meetgegevensRivm
	 */
	public MeetgegevensRivm getMeetgegevensRivm() {
		return meetgegevensRivm;
	}

	/**
	 * @param meetgegevensRivm the meetgegevensRivm to set
	 */
	public void setMeetgegevensRivm(MeetgegevensRivm meetgegevensRivm) {
		this.meetgegevensRivm = meetgegevensRivm;
	}

	/**
	 * @return the meetgegevensRivmLims
	 */
	public MeetgegevensRivmLims getMeetgegevensRivmLims() {
		return meetgegevensRivmLims;
	}

	/**
	 * @param meetgegevensRivmLims the meetgegevensRivmLims to set
	 */
	public void setMeetgegevensRivmLims(MeetgegevensRivmLims meetgegevensRivmLims) {
		this.meetgegevensRivmLims = meetgegevensRivmLims;
	}

	/**
	 * @return the meetgegevensRikilt
	 */
	public MeetgegevensRikilt getMeetgegevensRikilt() {
		return meetgegevensRikilt;
	}

	/**
	 * @param meetgegevensRikilt the meetgegevensRikilt to set
	 */
	public void setMeetgegevensRikilt(MeetgegevensRikilt meetgegevensRikilt) {
		this.meetgegevensRikilt = meetgegevensRikilt;
	}

	/**
	 * @return the meetgegevensRizaXml
	 */
	public MeetgegevensRizaXml getMeetgegevensRizaXml() {
		return meetgegevensRizaXml;
	}

	/**
	 * @param meetgegevensRizaXml the meetgegevensRizaXml to set
	 */
	public void setMeetgegevensRizaXml(MeetgegevensRizaXml meetgegevensRizaXml) {
		this.meetgegevensRizaXml = meetgegevensRizaXml;
	}

	/**
	 * @return the meetgegevensRizaExcel
	 */
	public MeetgegevensRizaExcel getMeetgegevensRizaExcel() {
		return meetgegevensRizaExcel;
	}

	/**
	 * @param meetgegevensRizaExcel the meetgegevensRizaExcel to set
	 */
	public void setMeetgegevensRizaExcel(MeetgegevensRizaExcel meetgegevensRizaExcel) {
		this.meetgegevensRizaExcel = meetgegevensRizaExcel;
	}

	/**
	 * @return the meetgegevensBrandweer
	 */
	public MeetgegevensBrandweer getMeetgegevensBrandweer() {
		return meetgegevensBrandweer;
	}

	/**
	 * @param meetgegevensBrandweer the meetgegevensBrandweer to set
	 */
	public void setMeetgegevensBrandweer(MeetgegevensBrandweer meetgegevensBrandweer) {
		this.meetgegevensBrandweer = meetgegevensBrandweer;
	}

	/**
	 * @return the meetgegevensVwa
	 */
	public MeetgegevensVwa getMeetgegevensVwa() {
		return meetgegevensVwa;
	}

	/**
	 * @param meetgegevensVwa the meetgegevensVwa to set
	 */
	public void setMeetgegevensVwa(MeetgegevensVwa meetgegevensVwa) {
		this.meetgegevensVwa = meetgegevensVwa;
	}

}

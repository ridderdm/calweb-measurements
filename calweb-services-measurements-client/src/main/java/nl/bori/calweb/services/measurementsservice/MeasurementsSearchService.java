/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import java.util.List;

import nl.bori.calweb.services.measurementsservice.model.Monster;
import nl.bori.calweb.services.measurementsservice.model.Substance;
import nl.bori.calweb.services.measurementsservice.search.MeetgegevensSearchControls;
import nl.bori.calweb.services.retrieval.RetrievalService;

/**
 * @author ridderdm
 *
 */
public interface MeasurementsSearchService extends RetrievalService<Monster, MeetgegevensSearchControls> {


	/**
	 * 
	 * @return een lijst met ingestuurde gemeten gegevens (nucliden, externe straling)
	 */
	List<String> getQuantities();
	
	
	/**
	 * 
	 * @return een lijst met ingestuurde substance
	 */
	List<Substance> getSubstances();
	
	
	/*
	 * 
	 * Deze service implementeert NIET de storage service.
	 * 
	 * De implementatie van de storage service zou betekenen dat de hele klasse hierarchie van Meetgegevens handmatig 
	 * doorgelopen moet worden om alle enititeiten (Bepaling, BepalingVWAs enz) te persisten voordat een perist uitgevoerd
	 * wordt op Meetgegevens.
	 * 
	 * De entities worden vanuit de database gegenereerd (de database bestond en is niet aangepast).  Bij de generatie
	 * van de entities wordt geen cascadetype gegenereerd, bij het persisten van de entity Meetgegevens leidt deze afwezigheid
	 * tot fouten omdat objecten waarnaar verwezen wordt null kunnen zijn.  
	 * 
	 * Bijvoorbeeld:
	 * Als direct een persist uitgevoerd wordt op Meetgegevens krijg je een exceptie die stelt dat Meetgegevens.afzender == null 
	 * terwijl dit object best in de hierarchie kan zitten en niet null is.  Daarom moeten we zelf de boom nalopen en alle 
	 * subentities zelf persisten.
	 * 
	 * Omdat het nalopen van alle entities die kunnen voorkomen veel werk is EN omdat bij het parsen van een inkomend bestand
	 * met metingen duidelijk is welke subentities bestaan, is ervoor gekozen om bij het parsen van het bestand de gegevens direct
	 * op te slaan.
	 */
}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "monster_brandweer")
public class MonsterBrandweer implements java.io.Serializable {

	private static final long serialVersionUID = -9042198485280794189L;

	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private int idMonster;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	private Monster monster;

	@Column(name = "meetploeg", nullable = false)
	private String meetploeg;

	@Column(name = "meetpunt", nullable = false)
	private String meetpunt;

	public MonsterBrandweer() {
	}

	public int getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(int idMonster) {
		this.idMonster = idMonster;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public String getMeetploeg() {
		return this.meetploeg;
	}

	public void setMeetploeg(String meetploeg) {
		this.meetploeg = meetploeg;
	}

	public String getMeetpunt() {
		return this.meetpunt;
	}

	public void setMeetpunt(String meetpunt) {
		this.meetpunt = meetpunt;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMonster;
		result = prime * result
				+ ((meetploeg == null) ? 0 : meetploeg.hashCode());
		result = prime * result
				+ ((meetpunt == null) ? 0 : meetpunt.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterBrandweer other = (MonsterBrandweer) obj;
		if (idMonster != other.idMonster)
			return false;
		if (meetploeg == null) {
			if (other.meetploeg != null)
				return false;
		} else if (!meetploeg.equals(other.meetploeg))
			return false;
		if (meetpunt == null) {
			if (other.meetpunt != null)
				return false;
		} else if (!meetpunt.equals(other.meetpunt))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterBrandweer [idMonster=" + idMonster + ", meetploeg="
				+ meetploeg + ", meetpunt=" + meetpunt + "]";
	}

}

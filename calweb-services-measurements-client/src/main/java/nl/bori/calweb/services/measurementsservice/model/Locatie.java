package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "locatie")
public class Locatie implements java.io.Serializable {

	private static final long serialVersionUID = 2915676101370496520L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	/**
	 * De x coordinaat van de locatie in RDM.
	 */
	@Column(name = "x_coordinaat", nullable = false)
	private int XCoordinaat;

	/**
	 * De y coordinaat van de locatie in RDM.
	 */
	@Column(name = "y_coordinaat", nullable = false)
	private int YCoordinaat;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "locatie")
	private List<Monster> monsters = new ArrayList<Monster>(0);

	public Locatie() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getXCoordinaat() {
		return this.XCoordinaat;
	}

	public void setXCoordinaat(int XCoordinaat) {
		this.XCoordinaat = XCoordinaat;
	}

	public int getYCoordinaat() {
		return this.YCoordinaat;
	}

	public void setYCoordinaat(int YCoordinaat) {
		this.YCoordinaat = YCoordinaat;
	}

	public List<Monster> getMonsters() {
		return this.monsters;
	}

	public void setMonsters(List<Monster> monsters) {
		this.monsters = monsters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + XCoordinaat;
		result = prime * result + YCoordinaat;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Locatie other = (Locatie) obj;
		if (XCoordinaat != other.XCoordinaat)
			return false;
		if (YCoordinaat != other.YCoordinaat)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Locatie [XCoordinaat=" + XCoordinaat + ", YCoordinaat="
				+ YCoordinaat + ", id=" + id + "]";
	}

}

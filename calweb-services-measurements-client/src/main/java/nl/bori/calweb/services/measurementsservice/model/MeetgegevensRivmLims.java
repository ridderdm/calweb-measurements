package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "meetgegevens_rivm_lims")
public class MeetgegevensRivmLims implements java.io.Serializable {

	private static final long serialVersionUID = 138373027256073392L;

	@Id
	@Column(name = "id_meetgegevens", unique = true, nullable = false)
	private int idMeetgegevens;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_meetgegevens", unique = true, nullable = false, insertable = false, updatable = false)
	private Meetgegevens meetgegevens;

	@Column(name = "sample_type")
	private String sampleType;

	@Column(name = "sample_name")
	private String sampleName;

	@Column(name = "sample_description")
	private String sampleDescription;

	@Column(name = "sample_identifier")
	private String sampleIdentifier;

	@Column(name = "sample_gamma")
	private String sampleGamma;

	@Column(name = "sourcesystem")
	private String sourcesystem;

	public MeetgegevensRivmLims() {
	}
	public int getIdMeetgegevens() {
		return this.idMeetgegevens;
	}

	public void setIdMeetgegevens(int idMeetgegevens) {
		this.idMeetgegevens = idMeetgegevens;
	}

	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	public String getSampleType() {
		return this.sampleType;
	}

	public void setSampleType(String sampleType) {
		this.sampleType = sampleType;
	}

	public String getSampleName() {
		return this.sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public String getSampleDescription() {
		return this.sampleDescription;
	}

	public void setSampleDescription(String sampleDescription) {
		this.sampleDescription = sampleDescription;
	}

	public String getSampleIdentifier() {
		return this.sampleIdentifier;
	}

	public void setSampleIdentifier(String sampleIdentifier) {
		this.sampleIdentifier = sampleIdentifier;
	}

	public String getSampleGamma() {
		return this.sampleGamma;
	}

	public void setSampleGamma(String sampleGamma) {
		this.sampleGamma = sampleGamma;
	}

	public String getSourcesystem() {
		return this.sourcesystem;
	}

	public void setSourcesystem(String sourcesystem) {
		this.sourcesystem = sourcesystem;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMeetgegevens;
		result = prime
				* result
				+ ((sampleDescription == null) ? 0 : sampleDescription
						.hashCode());
		result = prime * result
				+ ((sampleGamma == null) ? 0 : sampleGamma.hashCode());
		result = prime
				* result
				+ ((sampleIdentifier == null) ? 0 : sampleIdentifier.hashCode());
		result = prime * result
				+ ((sampleName == null) ? 0 : sampleName.hashCode());
		result = prime * result
				+ ((sampleType == null) ? 0 : sampleType.hashCode());
		result = prime * result
				+ ((sourcesystem == null) ? 0 : sourcesystem.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeetgegevensRivmLims other = (MeetgegevensRivmLims) obj;
		if (idMeetgegevens != other.idMeetgegevens)
			return false;
		if (sampleDescription == null) {
			if (other.sampleDescription != null)
				return false;
		} else if (!sampleDescription.equals(other.sampleDescription))
			return false;
		if (sampleGamma == null) {
			if (other.sampleGamma != null)
				return false;
		} else if (!sampleGamma.equals(other.sampleGamma))
			return false;
		if (sampleIdentifier == null) {
			if (other.sampleIdentifier != null)
				return false;
		} else if (!sampleIdentifier.equals(other.sampleIdentifier))
			return false;
		if (sampleName == null) {
			if (other.sampleName != null)
				return false;
		} else if (!sampleName.equals(other.sampleName))
			return false;
		if (sampleType == null) {
			if (other.sampleType != null)
				return false;
		} else if (!sampleType.equals(other.sampleType))
			return false;
		if (sourcesystem == null) {
			if (other.sourcesystem != null)
				return false;
		} else if (!sourcesystem.equals(other.sourcesystem))
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeetgegevensRivmLims [idMeetgegevens=" + idMeetgegevens
				+ ", sampleDescription=" + sampleDescription + ", sampleGamma="
				+ sampleGamma + ", sampleIdentifier=" + sampleIdentifier
				+ ", sampleName=" + sampleName + ", sampleType=" + sampleType
				+ ", sourcesystem=" + sourcesystem + "]";
	}

}

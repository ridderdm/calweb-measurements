package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "gemcentroide")
public class Gemcentroide implements java.io.Serializable {

	private static final long serialVersionUID = -7205925090902462402L;

	private int gemnr;

	private String gemnaam;

	private Date jaar;

	private int XCoordinaat;

	private int YCoordinaat;

	public Gemcentroide() {
	}

	public Gemcentroide(int gemnr, String gemnaam, Date jaar, int XCoordinaat,
			int YCoordinaat) {
		this.gemnr = gemnr;
		this.gemnaam = gemnaam;
		this.jaar = jaar;
		this.XCoordinaat = XCoordinaat;
		this.YCoordinaat = YCoordinaat;
	}

	@Id
	@Column(name = "gemnr", unique = true, nullable = false)
	public int getGemnr() {
		return this.gemnr;
	}

	public void setGemnr(int gemnr) {
		this.gemnr = gemnr;
	}

	@Column(name = "gemnaam", nullable = false)
	public String getGemnaam() {
		return this.gemnaam;
	}

	public void setGemnaam(String gemnaam) {
		this.gemnaam = gemnaam;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "jaar", nullable = false, length = 0)
	public Date getJaar() {
		return this.jaar;
	}

	public void setJaar(Date jaar) {
		this.jaar = jaar;
	}

	@Column(name = "x_coordinaat", nullable = false)
	public int getXCoordinaat() {
		return this.XCoordinaat;
	}

	public void setXCoordinaat(int XCoordinaat) {
		this.XCoordinaat = XCoordinaat;
	}

	@Column(name = "y_coordinaat", nullable = false)
	public int getYCoordinaat() {
		return this.YCoordinaat;
	}

	public void setYCoordinaat(int YCoordinaat) {
		this.YCoordinaat = YCoordinaat;
	}

}

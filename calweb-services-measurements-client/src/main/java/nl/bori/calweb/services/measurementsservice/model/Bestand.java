package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "bestand", uniqueConstraints = @UniqueConstraint(columnNames = "id_meetgegevens"))
public class Bestand implements java.io.Serializable {

	private static final long serialVersionUID = 5838943207915565491L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_meetgegevens", unique = true)
	private Meetgegevens meetgegevens;

	@Column(name = "naam", nullable = false)
	private String naam;

	@Column(name = "path", length = 225)
	private String path;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datum_upload", nullable = false, length = 19)
	private Date datumUpload;

	@Column(name = "orig_naam", length = 225)
	private String origNaam;

	@Column(name = "type", length = 225)
	private String type;

	public Bestand() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	public String getNaam() {
		return this.naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getDatumUpload() {
		return this.datumUpload;
	}

	public void setDatumUpload(Date datumUpload) {
		this.datumUpload = datumUpload;
	}

	public String getOrigNaam() {
		return this.origNaam;
	}

	public void setOrigNaam(String origNaam) {
		this.origNaam = origNaam;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}

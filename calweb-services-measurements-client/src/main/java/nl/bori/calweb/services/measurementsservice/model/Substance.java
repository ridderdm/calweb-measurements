/**
 * $Id:$
 */
package nl.bori.calweb.services.measurementsservice.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author ridderdm
 *
 */
@Entity
@Table(name = "substance")
public class Substance implements Serializable {

	private static final long serialVersionUID = -3305362236092183918L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "eurdep_code", unique = true, nullable = false)
	private String eurdep_code;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "substance")
	private List<Monster> monsters = new ArrayList<Monster>();

	public Substance() {
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the eurdep_code
	 */
	public String getEurdepCode() {
		return eurdep_code;
	}

	/**
	 * @param eurdepCode the eurdep_code to set
	 */
	public void setEurdepCode(String eurdepCode) {
		eurdep_code = eurdepCode;
	}

	/**
	 * @return the substance
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param substance the substance to set
	 */
	public void setName(String substance) {
		this.name = substance;
	}

	/**
	 * @param monsters the monsters to set
	 */
	public void setMonsters(List<Monster> monsters) {
		this.monsters = monsters;
	}

	/**
	 * @return the monsters
	 */
	public List<Monster> getMonsters() {
		return monsters;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((eurdep_code == null) ? 0 : eurdep_code.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Substance other = (Substance) obj;
		if (eurdep_code == null) {
			if (other.eurdep_code != null)
				return false;
		} else if (!eurdep_code.equals(other.eurdep_code))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Substance [eurdep_code=" + eurdep_code + ", id=" + id
				+ ", name=" + name + "]";
	}


	
	
}

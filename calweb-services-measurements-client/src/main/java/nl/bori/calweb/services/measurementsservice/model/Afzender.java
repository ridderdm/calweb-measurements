package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "afzender")
public class Afzender implements java.io.Serializable {

	private static final long serialVersionUID = -5132840313831603378L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_steuncentrum", nullable = false)
	private Centra centra;

	@Column(name = "naam", nullable = false, length = 100)
	private String naam;

	@Column(name = "telefoon", nullable = false, length = 15)
	private String telefoon;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "afzender")
	private List<Meetgegevens> meetgegevenses = new ArrayList<Meetgegevens>(0);

	public Afzender() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Centra getCentra() {
		return this.centra;
	}

	public void setCentra(Centra centra) {
		this.centra = centra;
	}

	public String getNaam() {
		return this.naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getTelefoon() {
		return this.telefoon;
	}

	public void setTelefoon(String telefoon) {
		this.telefoon = telefoon;
	}

	public List<Meetgegevens> getMeetgegevenses() {
		return this.meetgegevenses;
	}

	public void setMeetgegevenses(List<Meetgegevens> meetgegevenses) {
		this.meetgegevenses = meetgegevenses;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((centra == null) ? 0 : centra.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((naam == null) ? 0 : naam.hashCode());
		result = prime * result
				+ ((telefoon == null) ? 0 : telefoon.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Afzender other = (Afzender) obj;
		if (centra == null) {
			if (other.centra != null)
				return false;
		} else if (!centra.equals(other.centra))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (naam == null) {
			if (other.naam != null)
				return false;
		} else if (!naam.equals(other.naam))
			return false;
		if (telefoon == null) {
			if (other.telefoon != null)
				return false;
		} else if (!telefoon.equals(other.telefoon))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Afzender [centra=" + centra + ", id=" + id + ", naam=" + naam
				+ ", telefoon=" + telefoon + "]";
	}

}

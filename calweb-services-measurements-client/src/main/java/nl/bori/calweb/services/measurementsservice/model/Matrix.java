package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "matrix")
public class Matrix implements java.io.Serializable {

	private static final long serialVersionUID = -8995293523275883171L;

	@Id
	@Column(name = "matrixcode", unique = true, nullable = false)
	private int matrixcode;

	@Column(name = "omschrijving", nullable = false)
	private String omschrijving;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "matrix")
	private List<MonsterRizaExcel> monsterRizaExcels = new ArrayList<MonsterRizaExcel>(
			0);

	public Matrix() {
	}

	public int getMatrixcode() {
		return this.matrixcode;
	}

	public void setMatrixcode(int matrixcode) {
		this.matrixcode = matrixcode;
	}

	public String getOmschrijving() {
		return this.omschrijving;
	}

	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public List<MonsterRizaExcel> getMonsterRizaExcels() {
		return this.monsterRizaExcels;
	}

	public void setMonsterRizaExcels(List<MonsterRizaExcel> monsterRizaExcels) {
		this.monsterRizaExcels = monsterRizaExcels;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + matrixcode;
		result = prime * result
				+ ((omschrijving == null) ? 0 : omschrijving.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Matrix other = (Matrix) obj;
		if (matrixcode != other.matrixcode)
			return false;
		if (omschrijving == null) {
			if (other.omschrijving != null)
				return false;
		} else if (!omschrijving.equals(other.omschrijving))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Matrix [matrixcode=" + matrixcode + ", omschrijving="
				+ omschrijving + "]";
	}

}

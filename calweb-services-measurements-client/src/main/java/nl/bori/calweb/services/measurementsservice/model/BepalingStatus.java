package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "bepaling_status")
public class BepalingStatus implements java.io.Serializable {

	private static final long serialVersionUID = -466914806337762942L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_bepaling", nullable = false)
	private Bepaling bepaling;

	@Column(name = "status", nullable = false, length = 10)
	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datum", nullable = false, length = 19)
	private Date datum;

	@Column(name = "verantwoordelijke")
	private String verantwoordelijke;

	@Column(name = "reden")
	private String reden;

	public BepalingStatus() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Bepaling getBepaling() {
		return this.bepaling;
	}

	public void setBepaling(Bepaling bepaling) {
		this.bepaling = bepaling;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDatum() {
		return this.datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public String getVerantwoordelijke() {
		return this.verantwoordelijke;
	}

	public void setVerantwoordelijke(String verantwoordelijke) {
		this.verantwoordelijke = verantwoordelijke;
	}

	public String getReden() {
		return this.reden;
	}

	public void setReden(String reden) {
		this.reden = reden;
	}

}

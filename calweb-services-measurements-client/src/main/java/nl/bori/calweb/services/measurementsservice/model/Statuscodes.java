package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "statuscodes")
public class Statuscodes implements java.io.Serializable {

	private static final long serialVersionUID = -925812242148476593L;

	private int id;

	private String status;

	public Statuscodes() {
	}

	public Statuscodes(int id, String status) {
		this.id = id;
		this.status = status;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "status", nullable = false)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bepaling_rivm_lims")
public class BepalingRivmLims implements java.io.Serializable {

	private static final long serialVersionUID = 9165209102580901005L;

	@Id
	@Column(name = "id_bepaling", unique = true, nullable = false)
	private int idBepaling;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bepaling", unique = true, nullable = false, insertable = false, updatable = false)
	private Bepaling bepaling;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_eenheid_detectiegrens", nullable = false)
	private Eenheid eenheidByIdEenheidDetectiegrens;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_eenheid_onzekerheid", nullable = false)
	private Eenheid eenheidByIdEenheidOnzekerheid;

	@Column(name = "onzekerheid", nullable = false, precision = 12, scale = 0)
	private float onzekerheid;

	@Column(name = "detectiegrens", nullable = false, precision = 12, scale = 0)
	private float detectiegrens;

	@Column(name = "timelive", nullable = false, precision = 12, scale = 0)
	private float timelive;

	public BepalingRivmLims() {
	}

	public int getIdBepaling() {
		return this.idBepaling;
	}

	public void setIdBepaling(int idBepaling) {
		this.idBepaling = idBepaling;
	}

	public Bepaling getBepaling() {
		return this.bepaling;
	}

	public void setBepaling(Bepaling bepaling) {
		this.bepaling = bepaling;
	}

	public Eenheid getEenheidByIdEenheidDetectiegrens() {
		return this.eenheidByIdEenheidDetectiegrens;
	}

	public void setEenheidByIdEenheidDetectiegrens(
			Eenheid eenheidByIdEenheidDetectiegrens) {
		this.eenheidByIdEenheidDetectiegrens = eenheidByIdEenheidDetectiegrens;
	}

	public Eenheid getEenheidByIdEenheidOnzekerheid() {
		return this.eenheidByIdEenheidOnzekerheid;
	}

	public void setEenheidByIdEenheidOnzekerheid(
			Eenheid eenheidByIdEenheidOnzekerheid) {
		this.eenheidByIdEenheidOnzekerheid = eenheidByIdEenheidOnzekerheid;
	}

	public float getOnzekerheid() {
		return this.onzekerheid;
	}

	public void setOnzekerheid(float onzekerheid) {
		this.onzekerheid = onzekerheid;
	}

	public float getDetectiegrens() {
		return this.detectiegrens;
	}

	public void setDetectiegrens(float detectiegrens) {
		this.detectiegrens = detectiegrens;
	}

	public float getTimelive() {
		return this.timelive;
	}

	public void setTimelive(float timelive) {
		this.timelive = timelive;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(detectiegrens);
		result = prime
				* result
				+ ((eenheidByIdEenheidDetectiegrens == null) ? 0
						: eenheidByIdEenheidDetectiegrens.hashCode());
		result = prime
				* result
				+ ((eenheidByIdEenheidOnzekerheid == null) ? 0
						: eenheidByIdEenheidOnzekerheid.hashCode());
		result = prime * result + idBepaling;
		result = prime * result + Float.floatToIntBits(onzekerheid);
		result = prime * result + Float.floatToIntBits(timelive);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BepalingRivmLims other = (BepalingRivmLims) obj;
		if (Float.floatToIntBits(detectiegrens) != Float
				.floatToIntBits(other.detectiegrens))
			return false;
		if (eenheidByIdEenheidDetectiegrens == null) {
			if (other.eenheidByIdEenheidDetectiegrens != null)
				return false;
		} else if (!eenheidByIdEenheidDetectiegrens
				.equals(other.eenheidByIdEenheidDetectiegrens))
			return false;
		if (eenheidByIdEenheidOnzekerheid == null) {
			if (other.eenheidByIdEenheidOnzekerheid != null)
				return false;
		} else if (!eenheidByIdEenheidOnzekerheid
				.equals(other.eenheidByIdEenheidOnzekerheid))
			return false;
		if (idBepaling != other.idBepaling)
			return false;
		if (Float.floatToIntBits(onzekerheid) != Float
				.floatToIntBits(other.onzekerheid))
			return false;
		if (Float.floatToIntBits(timelive) != Float
				.floatToIntBits(other.timelive))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BepalingRivmLims [detectiegrens=" + detectiegrens
				+ ", eenheidByIdEenheidDetectiegrens="
				+ eenheidByIdEenheidDetectiegrens
				+ ", eenheidByIdEenheidOnzekerheid="
				+ eenheidByIdEenheidOnzekerheid + ", idBepaling=" + idBepaling
				+ ", onzekerheid=" + onzekerheid + ", timelive=" + timelive
				+ "]";
	}

}

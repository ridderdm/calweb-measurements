/**
 * 
 */
package nl.bori.calweb.services.measurementsservice.search;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import nl.bori.calweb.services.retrieval.SearchControls;

/**
 * @author ridderdm
 * 
 */
public class LocationSearchControls extends SearchControls {

	private static final long serialVersionUID = 5746727250440585866L;

	private int locationXBegin;
	private int locationXEnd;
	private int locationYBegin;
	private int locationYEnd;

	public void LocationSearchControl(int locationXBegin, int locationXEnd,
			int locationYBegin, int locationYEnd) {
		this.locationXBegin = locationXBegin;
		this.locationXEnd = locationXEnd;
		this.locationYBegin = locationYBegin;
		this.locationYEnd = locationYEnd;
	}

	public void LocationSearchControl(double locationXBegin, double locationXEnd,
			double locationYBegin, double locationYEnd) {
		this.locationXBegin = (int) locationXBegin;
		this.locationXEnd = (int) locationXEnd;
		this.locationYBegin = (int) locationYBegin;
		this.locationYEnd = (int) locationYEnd;
	}

	public int getLocationXBegin() {
		return locationXBegin;
	}

	public void setLocationXBegin(int locationXBegin) {
		this.locationXBegin = locationXBegin;
	}

	public int getLocationXEnd() {
		return locationXEnd;
	}

	public void setLocationXEnd(int locationXEnd) {
		this.locationXEnd = locationXEnd;
	}

	public int getLocationYBegin() {
		return locationYBegin;
	}

	public void setLocationYBegin(int locationYBegin) {
		this.locationYBegin = locationYBegin;
	}

	public int getLocationYEnd() {
		return locationYEnd;
	}

	public void setLocationYEnd(int locationYEnd) {
		this.locationYEnd = locationYEnd;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(locationXBegin)
				.append(locationXEnd).append(locationYBegin).append(
						locationYEnd).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}

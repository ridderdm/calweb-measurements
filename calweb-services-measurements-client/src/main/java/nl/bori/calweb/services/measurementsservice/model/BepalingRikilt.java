package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bepaling_rikilt")
public class BepalingRikilt implements java.io.Serializable {

	private static final long serialVersionUID = 4235288521266472255L;

	@Id
	@Column(name = "id_bepaling", unique = true, nullable = false)
	private int idBepaling;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bepaling", unique = true, nullable = false, insertable = false, updatable = false)
	private Bepaling bepaling;

	@Column(name = "afwijking", nullable = false, precision = 12, scale = 0)
	private float afwijking;

	public BepalingRikilt() {
	}

	public int getIdBepaling() {
		return this.idBepaling;
	}

	public void setIdBepaling(int idBepaling) {
		this.idBepaling = idBepaling;
	}

	public Bepaling getBepaling() {
		return this.bepaling;
	}

	public void setBepaling(Bepaling bepaling) {
		this.bepaling = bepaling;
	}

	public float getAfwijking() {
		return this.afwijking;
	}

	public void setAfwijking(float afwijking) {
		this.afwijking = afwijking;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(afwijking);
		result = prime * result + idBepaling;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BepalingRikilt other = (BepalingRikilt) obj;
		if (Float.floatToIntBits(afwijking) != Float
				.floatToIntBits(other.afwijking))
			return false;
		if (idBepaling != other.idBepaling)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BepalingRikilt [afwijking=" + afwijking + ", idBepaling="
				+ idBepaling + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class DeelcompartimentInfoId implements java.io.Serializable {

	private static final long serialVersionUID = -5052776287753260374L;

	private int compnr;

	private int nummer;

	public DeelcompartimentInfoId() {
	}

	public DeelcompartimentInfoId(int compnr, int nummer) {
		this.compnr = compnr;
		this.nummer = nummer;
	}

	@Column(name = "compnr", nullable = false)
	public int getCompnr() {
		return this.compnr;
	}

	public void setCompnr(int compnr) {
		this.compnr = compnr;
	}

	@Column(name = "nummer", nullable = false)
	public int getNummer() {
		return this.nummer;
	}

	public void setNummer(int nummer) {
		this.nummer = nummer;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof DeelcompartimentInfoId))
			return false;
		DeelcompartimentInfoId castOther = (DeelcompartimentInfoId) other;

		return (this.getCompnr() == castOther.getCompnr())
				&& (this.getNummer() == castOther.getNummer());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getCompnr();
		result = 37 * result + this.getNummer();
		return result;
	}

}

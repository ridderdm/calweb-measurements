/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice.search;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import nl.bori.calweb.services.retrieval.SearchControls;

/**
 * @author ridderdm
 * 
 *         Klasse die zoekt op attributen van de Monster klasse
 */
public class MonsterSearchControls extends SearchControls {

	private static final long serialVersionUID = 6098206472720086581L;

	private static final Logger logger = Logger.getLogger(MonsterSearchControls.class);

	private BepalingSearchControls bepalingSearchControls = new BepalingSearchControls();

	private LocationSearchControls locationSearchControls = null;
	
	/**
	 * Monstername begintijd
	 */
	private DateTime startDate = null;

	/**
	 * Monstername eindtijd
	 */
	private DateTime endDate = null;

	/**
	 * De eurdep codes van de substances waarnaar gezocht moet worden.
	 */
	private List<String> substance_codes = new ArrayList<String>(0);
	
	public MonsterSearchControls() {
		super();
	}

	public MonsterSearchControls(DateTime startDate, DateTime endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * @return the startDate
	 */
	public DateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(DateTime startDate) {
		if (logger.isDebugEnabled() && startDate != null) {
			logger.debug("setStartDate(" + startDate.toString() + ")");
		}
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		if (logger.isDebugEnabled() && endDate != null) {
			logger.debug("setEndDate(" + endDate.toString() + ")");
		}
		this.endDate = endDate;
	}

	public List<String> getSubstanceCodes() {
		return substance_codes;
	}

	public void setSubstanceCodes(List<String> substanceCodes) {
		this.substance_codes = substanceCodes;
	}
	
	public void addSubstanceCode(String substanceCode) {
		this.substance_codes.add(substanceCode);
	}

	/**
	 * @return the bepalingSearchControls
	 */
	public BepalingSearchControls getBepalingSearchControls() {
		return bepalingSearchControls;
	}

	/**
	 * @param bepalingSearchControls
	 *            the bepalingSearchControls to set
	 */
	public void setBepalingSearchControls(
			BepalingSearchControls bepalingSearchControls) {
		this.bepalingSearchControls = bepalingSearchControls;
	}

	public void setLocationSearchControls(LocationSearchControls locationSearchControls) {
		this.locationSearchControls = locationSearchControls;
	}

	public LocationSearchControls getLocationSearchControls() {
		return locationSearchControls;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((bepalingSearchControls == null) ? 0
						: bepalingSearchControls.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime
				* result
				+ ((locationSearchControls == null) ? 0
						: locationSearchControls.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result
				+ ((substance_codes == null) ? 0 : substance_codes.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterSearchControls other = (MonsterSearchControls) obj;
		if (bepalingSearchControls == null) {
			if (other.bepalingSearchControls != null)
				return false;
		} else if (!bepalingSearchControls.equals(other.bepalingSearchControls))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (locationSearchControls == null) {
			if (other.locationSearchControls != null)
				return false;
		} else if (!locationSearchControls.equals(other.locationSearchControls))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (substance_codes == null) {
			if (other.substance_codes != null)
				return false;
		} else if (!substance_codes.equals(other.substance_codes))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterSearchControls [bepalingSearchControls="
				+ bepalingSearchControls + ", endDate=" + endDate
				+ ", locationSearchControls=" + locationSearchControls
				+ ", startDate=" + startDate + ", substance_codes="
				+ substance_codes + "]";
	}



}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "monster_riza_xml")
public class MonsterRizaXml implements java.io.Serializable {

	private static final long serialVersionUID = 7998102945890617875L;

	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private int idMonster;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	private Monster monster;

	@Column(name = "nummer", nullable = false, length = 15)
	private String nummer;

	@Column(name = "gebieds_codering", nullable = false, length = 100)
	private String gebiedsCodering;

	@Column(name = "gebieds_omschrijving", nullable = false)
	private String gebiedsOmschrijving;

	@Column(name = "locatie_codering", nullable = false, length = 100)
	private String locatieCodering;

	@Column(name = "locatie_omschrijving", nullable = false)
	private String locatieOmschrijving;

	@Column(name = "compartiment_codering", nullable = false, length = 2)
	private String compartimentCodering;

	@Column(name = "compartiment_omschrijving", nullable = false, length = 100)
	private String compartimentOmschrijving;

	public MonsterRizaXml() {
	}
	
	public int getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(int idMonster) {
		this.idMonster = idMonster;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public String getNummer() {
		return this.nummer;
	}

	public void setNummer(String nummer) {
		this.nummer = nummer;
	}

	public String getGebiedsCodering() {
		return this.gebiedsCodering;
	}

	public void setGebiedsCodering(String gebiedsCodering) {
		this.gebiedsCodering = gebiedsCodering;
	}

	public String getGebiedsOmschrijving() {
		return this.gebiedsOmschrijving;
	}

	public void setGebiedsOmschrijving(String gebiedsOmschrijving) {
		this.gebiedsOmschrijving = gebiedsOmschrijving;
	}

	public String getLocatieCodering() {
		return this.locatieCodering;
	}

	public void setLocatieCodering(String locatieCodering) {
		this.locatieCodering = locatieCodering;
	}

	public String getLocatieOmschrijving() {
		return this.locatieOmschrijving;
	}

	public void setLocatieOmschrijving(String locatieOmschrijving) {
		this.locatieOmschrijving = locatieOmschrijving;
	}

	public String getCompartimentCodering() {
		return this.compartimentCodering;
	}

	public void setCompartimentCodering(String compartimentCodering) {
		this.compartimentCodering = compartimentCodering;
	}

	public String getCompartimentOmschrijving() {
		return this.compartimentOmschrijving;
	}

	public void setCompartimentOmschrijving(String compartimentOmschrijving) {
		this.compartimentOmschrijving = compartimentOmschrijving;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((compartimentCodering == null) ? 0 : compartimentCodering
						.hashCode());
		result = prime
				* result
				+ ((compartimentOmschrijving == null) ? 0
						: compartimentOmschrijving.hashCode());
		result = prime * result
				+ ((gebiedsCodering == null) ? 0 : gebiedsCodering.hashCode());
		result = prime
				* result
				+ ((gebiedsOmschrijving == null) ? 0 : gebiedsOmschrijving
						.hashCode());
		result = prime * result + idMonster;
		result = prime * result
				+ ((locatieCodering == null) ? 0 : locatieCodering.hashCode());
		result = prime
				* result
				+ ((locatieOmschrijving == null) ? 0 : locatieOmschrijving
						.hashCode());
		result = prime * result + ((nummer == null) ? 0 : nummer.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterRizaXml other = (MonsterRizaXml) obj;
		if (compartimentCodering == null) {
			if (other.compartimentCodering != null)
				return false;
		} else if (!compartimentCodering.equals(other.compartimentCodering))
			return false;
		if (compartimentOmschrijving == null) {
			if (other.compartimentOmschrijving != null)
				return false;
		} else if (!compartimentOmschrijving
				.equals(other.compartimentOmschrijving))
			return false;
		if (gebiedsCodering == null) {
			if (other.gebiedsCodering != null)
				return false;
		} else if (!gebiedsCodering.equals(other.gebiedsCodering))
			return false;
		if (gebiedsOmschrijving == null) {
			if (other.gebiedsOmschrijving != null)
				return false;
		} else if (!gebiedsOmschrijving.equals(other.gebiedsOmschrijving))
			return false;
		if (idMonster != other.idMonster)
			return false;
		if (locatieCodering == null) {
			if (other.locatieCodering != null)
				return false;
		} else if (!locatieCodering.equals(other.locatieCodering))
			return false;
		if (locatieOmschrijving == null) {
			if (other.locatieOmschrijving != null)
				return false;
		} else if (!locatieOmschrijving.equals(other.locatieOmschrijving))
			return false;
		if (nummer == null) {
			if (other.nummer != null)
				return false;
		} else if (!nummer.equals(other.nummer))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterRizaXml [compartimentCodering=" + compartimentCodering
				+ ", compartimentOmschrijving=" + compartimentOmschrijving
				+ ", gebiedsCodering=" + gebiedsCodering
				+ ", gebiedsOmschrijving=" + gebiedsOmschrijving
				+ ", idMonster=" + idMonster + ", locatieCodering="
				+ locatieCodering + ", locatieOmschrijving="
				+ locatieOmschrijving + ", nummer=" + nummer + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "meetgegevens_rikilt")
public class MeetgegevensRikilt implements java.io.Serializable {

	private static final long serialVersionUID = -6810297680466737819L;

	@Id
	@Column(name = "id_meetgegevens", unique = true, nullable = false)
	private int idMeetgegevens;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_meetgegevens", unique = true, nullable = false, insertable = false, updatable = false)
	private Meetgegevens meetgegevens;

	@Column(name = "verzendnummer", nullable = false)
	private int verzendnummer;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "aanmaakdatum", nullable = false, length = 19)
	private Date aanmaakdatum;

	public MeetgegevensRikilt() {
	}

	public int getIdMeetgegevens() {
		return this.idMeetgegevens;
	}

	public void setIdMeetgegevens(int idMeetgegevens) {
		this.idMeetgegevens = idMeetgegevens;
	}

	public Meetgegevens getMeetgegevens() {
		return this.meetgegevens;
	}

	public void setMeetgegevens(Meetgegevens meetgegevens) {
		this.meetgegevens = meetgegevens;
	}

	public int getVerzendnummer() {
		return this.verzendnummer;
	}

	public void setVerzendnummer(int verzendnummer) {
		this.verzendnummer = verzendnummer;
	}

	public Date getAanmaakdatum() {
		return this.aanmaakdatum;
	}

	public void setAanmaakdatum(Date aanmaakdatum) {
		this.aanmaakdatum = aanmaakdatum;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((aanmaakdatum == null) ? 0 : aanmaakdatum.hashCode());
		result = prime * result + idMeetgegevens;
		result = prime * result + verzendnummer;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeetgegevensRikilt other = (MeetgegevensRikilt) obj;
		if (aanmaakdatum == null) {
			if (other.aanmaakdatum != null)
				return false;
		} else if (!aanmaakdatum.equals(other.aanmaakdatum))
			return false;
		if (idMeetgegevens != other.idMeetgegevens)
			return false;
		if (verzendnummer != other.verzendnummer)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeetgegevensRikilt [aanmaakdatum=" + aanmaakdatum
				+ ", idMeetgegevens=" + idMeetgegevens + ", verzendnummer="
				+ verzendnummer + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "monster_rivm")
public class MonsterRivm implements java.io.Serializable {

	private static final long serialVersionUID = -3389238884516463072L;

	/*
	 * TODO uitzoeken waarom hier EN een id_monster EN een monster variabele gebruikt wordt.  Eigenlijk
	 * zou een monster voldoende moeten zijn en zou de id_monster afgeleid moeten worden van monster.
	 * Haal je echter de id_monster weg, krijg je foutmeldingen.
	 * 
	 */
	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private int idMonster;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	private Monster monster;

	@Column(name = "id_wvimeting", nullable = false)
	private int idWvimeting;

	@Column(name = "lokatiebeschrijving")
	private String lokatiebeschrijving;

	@Column(name = "volume", nullable = false, precision = 12, scale = 0)
	private float volume;

	@Column(name = "beschrijving", nullable = false)
	private String beschrijving;

	public MonsterRivm() {
	}

	public int getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(int idMonster) {
		this.idMonster = idMonster;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public int getIdWvimeting() {
		return this.idWvimeting;
	}

	public void setIdWvimeting(int idWvimeting) {
		this.idWvimeting = idWvimeting;
	}

	public String getLokatiebeschrijving() {
		return this.lokatiebeschrijving;
	}

	public void setLokatiebeschrijving(String lokatiebeschrijving) {
		this.lokatiebeschrijving = lokatiebeschrijving;
	}

	public float getVolume() {
		return this.volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public String getBeschrijving() {
		return this.beschrijving;
	}

	public void setBeschrijving(String beschrijving) {
		this.beschrijving = beschrijving;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((beschrijving == null) ? 0 : beschrijving.hashCode());
		result = prime * result + idMonster;
		result = prime * result + idWvimeting;
		result = prime
				* result
				+ ((lokatiebeschrijving == null) ? 0 : lokatiebeschrijving
						.hashCode());
		result = prime * result + Float.floatToIntBits(volume);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterRivm other = (MonsterRivm) obj;
		if (beschrijving == null) {
			if (other.beschrijving != null)
				return false;
		} else if (!beschrijving.equals(other.beschrijving))
			return false;
		if (idMonster != other.idMonster)
			return false;
		if (idWvimeting != other.idWvimeting)
			return false;
		if (lokatiebeschrijving == null) {
			if (other.lokatiebeschrijving != null)
				return false;
		} else if (!lokatiebeschrijving.equals(other.lokatiebeschrijving))
			return false;
		if (Float.floatToIntBits(volume) != Float.floatToIntBits(other.volume))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterRivm [beschrijving=" + beschrijving + ", idMonster="
				+ idMonster + ", idWvimeting=" + idWvimeting
				+ ", lokatiebeschrijving=" + lokatiebeschrijving + ", volume="
				+ volume + "]";
	}

}

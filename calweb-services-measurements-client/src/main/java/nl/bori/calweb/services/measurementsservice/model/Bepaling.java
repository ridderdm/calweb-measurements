package nl.bori.calweb.services.measurementsservice.model;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "bepaling")
public class Bepaling implements java.io.Serializable {

	private static final long serialVersionUID = 3461236847950643952L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_monster", nullable = false)
	private Monster monster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_eenheid", nullable = false)
	private Eenheid eenheid;

	@Column(name = "nuclide", nullable = false, length = 7)
	private String nuclide;

	@Column(name = "meetwaarde", nullable = false, precision = 22, scale = 0)
	private double meetwaarde;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bepaling_begin", nullable = false, length = 19)
	private Date bepalingBegin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bepaling_eind", nullable = false, length = 19)
	private Date bepalingEind;

	@Column(name = "opgestuurde_nuclide", nullable = false, length = 100)
	private String opgestuurdeNuclide;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "bepaling")
	private List<BepalingStatus> bepalingStatuses = new ArrayList<BepalingStatus>(0);

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bepaling")
	private BepalingVwa bepalingVwa;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bepaling")
	private BepalingRivm bepalingRivm;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bepaling")
	private BepalingRizaExcel bepalingRizaExcel;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bepaling")
	private BepalingRizaXml bepalingRizaXml;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bepaling")
	private BepalingRivmLims bepalingRivmLims;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "bepaling")
	private BepalingRikilt bepalingRikilt;
	
	public Bepaling() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public Eenheid getEenheid() {
		return this.eenheid;
	}

	public void setEenheid(Eenheid eenheid) {
		this.eenheid = eenheid;
	}

	public String getNuclide() {
		return this.nuclide;
	}

	public void setNuclide(String nuclide) {
		this.nuclide = nuclide;
	}

	public double getMeetwaarde() {
		return this.meetwaarde;
	}

	public void setMeetwaarde(double meetwaarde) {
		this.meetwaarde = meetwaarde;
	}

	public Date getBepalingBegin() {
		return this.bepalingBegin;
	}

	public void setBepalingBegin(Date bepalingBegin) {
		this.bepalingBegin = bepalingBegin;
	}

	public Date getBepalingEind() {
		return this.bepalingEind;
	}

	public void setBepalingEind(Date bepalingEind) {
		this.bepalingEind = bepalingEind;
	}

	public String getOpgestuurdeNuclide() {
		return this.opgestuurdeNuclide;
	}

	public void setOpgestuurdeNuclide(String opgestuurdeNuclide) {
		this.opgestuurdeNuclide = opgestuurdeNuclide;
	}


	public List<BepalingStatus> getBepalingStatuses() {
		return this.bepalingStatuses;
	}

	public void setBepalingStatuses(List<BepalingStatus> bepalingStatuses) {
		this.bepalingStatuses = bepalingStatuses;
	}

	/**
	 * @return the bepalingVwa
	 */
	public BepalingVwa getBepalingVwa() {
		return bepalingVwa;
	}

	/**
	 * @param bepalingVwa the bepalingVwa to set
	 */
	public void setBepalingVwa(BepalingVwa bepalingVwa) {
		this.bepalingVwa = bepalingVwa;
	}

	/**
	 * @return the bepalingRivm
	 */
	public BepalingRivm getBepalingRivm() {
		return bepalingRivm;
	}

	/**
	 * @param bepalingRivm the bepalingRivm to set
	 */
	public void setBepalingRivm(BepalingRivm bepalingRivm) {
		this.bepalingRivm = bepalingRivm;
	}

	/**
	 * @return the bepalingRizaExcel
	 */
	public BepalingRizaExcel getBepalingRizaExcel() {
		return bepalingRizaExcel;
	}

	/**
	 * @param bepalingRizaExcel the bepalingRizaExcel to set
	 */
	public void setBepalingRizaExcel(BepalingRizaExcel bepalingRizaExcel) {
		this.bepalingRizaExcel = bepalingRizaExcel;
	}

	/**
	 * @return the bepalingRizaXml
	 */
	public BepalingRizaXml getBepalingRizaXml() {
		return bepalingRizaXml;
	}


	/**
	 * @param bepalingRizaXml the bepalingRizaXml to set
	 */
	public void setBepalingRizaXml(BepalingRizaXml bepalingRizaXml) {
		this.bepalingRizaXml = bepalingRizaXml;
	}

	/**
	 * @return the bepalingRivmLims
	 */
	public BepalingRivmLims getBepalingRivmLims() {
		return bepalingRivmLims;
	}

	/**
	 * @param bepalingRivmLims the bepalingRivmLims to set
	 */
	public void setBepalingRivmLims(BepalingRivmLims bepalingRivmLims) {
		this.bepalingRivmLims = bepalingRivmLims;
	}

	/**
	 * @return the bepalingRikilt
	 */
	public BepalingRikilt getBepalingRikilt() {
		return bepalingRikilt;
	}

	/**
	 * @param bepalingRikilt the bepalingRikilt to set
	 */
	public void setBepalingRikilt(BepalingRikilt bepalingRikilt) {
		this.bepalingRikilt = bepalingRikilt;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((bepalingBegin == null) ? 0 : bepalingBegin.hashCode());
		result = prime * result
				+ ((bepalingEind == null) ? 0 : bepalingEind.hashCode());
		result = prime * result + ((eenheid == null) ? 0 : eenheid.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		long temp;
		temp = Double.doubleToLongBits(meetwaarde);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((nuclide == null) ? 0 : nuclide.hashCode());
		result = prime
				* result
				+ ((opgestuurdeNuclide == null) ? 0 : opgestuurdeNuclide
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bepaling other = (Bepaling) obj;
		if (bepalingBegin == null) {
			if (other.bepalingBegin != null)
				return false;
		} else if (!bepalingBegin.equals(other.bepalingBegin))
			return false;
		if (bepalingEind == null) {
			if (other.bepalingEind != null)
				return false;
		} else if (!bepalingEind.equals(other.bepalingEind))
			return false;
		if (eenheid == null) {
			if (other.eenheid != null)
				return false;
		} else if (!eenheid.equals(other.eenheid))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (Double.doubleToLongBits(meetwaarde) != Double
				.doubleToLongBits(other.meetwaarde))
			return false;
		if (nuclide == null) {
			if (other.nuclide != null)
				return false;
		} else if (!nuclide.equals(other.nuclide))
			return false;
		if (opgestuurdeNuclide == null) {
			if (other.opgestuurdeNuclide != null)
				return false;
		} else if (!opgestuurdeNuclide.equals(other.opgestuurdeNuclide))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bepaling [bepalingBegin=" + bepalingBegin + ", bepalingEind="
				+ bepalingEind + ", bepalingRikilt=" + bepalingRikilt
				+ ", bepalingRivm=" + bepalingRivm + ", bepalingRivmLims="
				+ bepalingRivmLims + ", bepalingRizaExcel=" + bepalingRizaExcel
				+ ", bepalingRizaXml=" + bepalingRizaXml
				+ ", bepalingStatuses=" + bepalingStatuses + ", bepalingVwa="
				+ bepalingVwa + ", eenheid=" + eenheid + ", id=" + id
				+ ", meetwaarde=" + meetwaarde + ", nuclide=" + nuclide
				+ ", opgestuurdeNuclide=" + opgestuurdeNuclide + "]";
	}

}

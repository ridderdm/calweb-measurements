package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "compartiment")
public class Compartiment implements java.io.Serializable {

	private static final long serialVersionUID = 4677978642570551753L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns( {
			@JoinColumn(name = "comp", referencedColumnName = "compnr", nullable = false),
			@JoinColumn(name = "deelcomp", referencedColumnName = "deelcompnr", nullable = false),
			@JoinColumn(name = "subdeelcomp", referencedColumnName = "nummer", nullable = false) })
	private SubdeelcompartimentInfo subdeelcompartimentInfo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "compartiment")
	private List<MonsterRikilt> monsterRikilts = new ArrayList<MonsterRikilt>(0);

	public Compartiment() {
	}

	public Compartiment(SubdeelcompartimentInfo subdeelcompartimentInfo) {
		this.subdeelcompartimentInfo = subdeelcompartimentInfo;
	}

	public Compartiment(SubdeelcompartimentInfo subdeelcompartimentInfo,
			List<MonsterRikilt> monsterRikilts) {
		this.subdeelcompartimentInfo = subdeelcompartimentInfo;
		this.monsterRikilts = monsterRikilts;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SubdeelcompartimentInfo getSubdeelcompartimentInfo() {
		return this.subdeelcompartimentInfo;
	}

	public void setSubdeelcompartimentInfo(
			SubdeelcompartimentInfo subdeelcompartimentInfo) {
		this.subdeelcompartimentInfo = subdeelcompartimentInfo;
	}

	public List<MonsterRikilt> getMonsterRikilts() {
		return this.monsterRikilts;
	}

	public void setMonsterRikilts(List<MonsterRikilt> monsterRikilts) {
		this.monsterRikilts = monsterRikilts;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((subdeelcompartimentInfo == null) ? 0
						: subdeelcompartimentInfo.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compartiment other = (Compartiment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (subdeelcompartimentInfo == null) {
			if (other.subdeelcompartimentInfo != null)
				return false;
		} else if (!subdeelcompartimentInfo
				.equals(other.subdeelcompartimentInfo))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Compartiment [id=" + id + ", subdeelcompartimentInfo="
				+ subdeelcompartimentInfo + "]";
	}

}

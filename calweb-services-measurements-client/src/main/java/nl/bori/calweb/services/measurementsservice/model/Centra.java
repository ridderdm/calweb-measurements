package nl.bori.calweb.services.measurementsservice.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "centra", uniqueConstraints = @UniqueConstraint(columnNames = "uid"))
public class Centra implements java.io.Serializable {

	private static final long serialVersionUID = -7268422857892454699L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "afkorting", length = 12)
	private String afkorting;

	@Column(name = "naam", nullable = false)
	private String naam;

	@Column(name = "korte_naam", nullable = false, length = 40)
	private String korteNaam;

	@Column(name = "webadres", nullable = false)
	private String webadres;

	@Column(name = "type", nullable = false, length = 18)
	private String type;

	@Column(name = "omschrijving", length = 65535)
	private String omschrijving;

	@Column(name = "rd_x", precision = 12, scale = 0)
	private Float rdX;

	@Column(name = "rd_y", precision = 12, scale = 0)
	private Float rdY;

	@Column(name = "uid", unique = true, length = 50)
	private String uid;

	@Column(name = "logo", nullable = false, length = 4)
	private String logo;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "centra")
	private List<Afzender> afzenders = new ArrayList<Afzender>(0);

	public Centra() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAfkorting() {
		return this.afkorting;
	}

	public void setAfkorting(String afkorting) {
		this.afkorting = afkorting;
	}

	public String getNaam() {
		return this.naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getKorteNaam() {
		return this.korteNaam;
	}

	public void setKorteNaam(String korteNaam) {
		this.korteNaam = korteNaam;
	}

	public String getWebadres() {
		return this.webadres;
	}

	public void setWebadres(String webadres) {
		this.webadres = webadres;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOmschrijving() {
		return this.omschrijving;
	}

	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public Float getRdX() {
		return this.rdX;
	}

	public void setRdX(Float rdX) {
		this.rdX = rdX;
	}

	public Float getRdY() {
		return this.rdY;
	}

	public void setRdY(Float rdY) {
		this.rdY = rdY;
	}

	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public List<Afzender> getAfzenders() {
		return this.afzenders;
	}

	public void setAfzenders(List<Afzender> afzenders) {
		this.afzenders = afzenders;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((afkorting == null) ? 0 : afkorting.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((korteNaam == null) ? 0 : korteNaam.hashCode());
		result = prime * result + ((logo == null) ? 0 : logo.hashCode());
		result = prime * result + ((naam == null) ? 0 : naam.hashCode());
		result = prime * result
				+ ((omschrijving == null) ? 0 : omschrijving.hashCode());
		result = prime * result + ((rdX == null) ? 0 : rdX.hashCode());
		result = prime * result + ((rdY == null) ? 0 : rdY.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((uid == null) ? 0 : uid.hashCode());
		result = prime * result
				+ ((webadres == null) ? 0 : webadres.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Centra other = (Centra) obj;
		if (afkorting == null) {
			if (other.afkorting != null)
				return false;
		} else if (!afkorting.equals(other.afkorting))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (korteNaam == null) {
			if (other.korteNaam != null)
				return false;
		} else if (!korteNaam.equals(other.korteNaam))
			return false;
		if (logo == null) {
			if (other.logo != null)
				return false;
		} else if (!logo.equals(other.logo))
			return false;
		if (naam == null) {
			if (other.naam != null)
				return false;
		} else if (!naam.equals(other.naam))
			return false;
		if (omschrijving == null) {
			if (other.omschrijving != null)
				return false;
		} else if (!omschrijving.equals(other.omschrijving))
			return false;
		if (rdX == null) {
			if (other.rdX != null)
				return false;
		} else if (!rdX.equals(other.rdX))
			return false;
		if (rdY == null) {
			if (other.rdY != null)
				return false;
		} else if (!rdY.equals(other.rdY))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (uid == null) {
			if (other.uid != null)
				return false;
		} else if (!uid.equals(other.uid))
			return false;
		if (webadres == null) {
			if (other.webadres != null)
				return false;
		} else if (!webadres.equals(other.webadres))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Centra [afkorting=" + afkorting + ", id=" + id + ", korteNaam="
				+ korteNaam + ", logo=" + logo + ", naam=" + naam
				+ ", omschrijving=" + omschrijving + ", rdX=" + rdX + ", rdY="
				+ rdY + ", type=" + type + ", uid=" + uid + ", webadres="
				+ webadres + "]";
	}

}

package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "monster_riza_excel")
public class MonsterRizaExcel implements java.io.Serializable {

	private static final long serialVersionUID = -666874959779505877L;

	@Id
	@Column(name = "id_monster", unique = true, nullable = false)
	private int idMonster;

	@JoinColumn(name = "id_monster", unique = true, nullable = false, insertable = false, updatable = false)
	@OneToOne(fetch = FetchType.EAGER)
	private Monster monster;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_wdlocatie")
	private WaterdienstLocaties waterdienstLocaties;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_matrix")
	private Matrix matrix;

	@Column(name = "LIMS_ID", nullable = false)
	private int limsId;

	@Column(name = "locatie")
	private String locatie;

	public MonsterRizaExcel() {
	}

	public int getIdMonster() {
		return this.idMonster;
	}

	public void setIdMonster(int idMonster) {
		this.idMonster = idMonster;
	}

	public WaterdienstLocaties getWaterdienstLocaties() {
		return this.waterdienstLocaties;
	}

	public void setWaterdienstLocaties(WaterdienstLocaties waterdienstLocaties) {
		this.waterdienstLocaties = waterdienstLocaties;
	}

	public Matrix getMatrix() {
		return this.matrix;
	}

	public void setMatrix(Matrix matrix) {
		this.matrix = matrix;
	}

	public Monster getMonster() {
		return this.monster;
	}

	public void setMonster(Monster monster) {
		this.monster = monster;
	}

	public int getLimsId() {
		return this.limsId;
	}

	public void setLimsId(int limsId) {
		this.limsId = limsId;
	}

	public String getLocatie() {
		return this.locatie;
	}

	public void setLocatie(String locatie) {
		this.locatie = locatie;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idMonster;
		result = prime * result + limsId;
		result = prime * result + ((locatie == null) ? 0 : locatie.hashCode());
		result = prime * result + ((matrix == null) ? 0 : matrix.hashCode());
		result = prime
				* result
				+ ((waterdienstLocaties == null) ? 0 : waterdienstLocaties
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MonsterRizaExcel other = (MonsterRizaExcel) obj;
		if (idMonster != other.idMonster)
			return false;
		if (limsId != other.limsId)
			return false;
		if (locatie == null) {
			if (other.locatie != null)
				return false;
		} else if (!locatie.equals(other.locatie))
			return false;
		if (matrix == null) {
			if (other.matrix != null)
				return false;
		} else if (!matrix.equals(other.matrix))
			return false;
		if (waterdienstLocaties == null) {
			if (other.waterdienstLocaties != null)
				return false;
		} else if (!waterdienstLocaties.equals(other.waterdienstLocaties))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MonsterRizaExcel [idMonster=" + idMonster + ", limsId="
				+ limsId + ", locatie=" + locatie + ", matrix=" + matrix
				+ ", waterdienstLocaties=" + waterdienstLocaties + "]";
	}

}

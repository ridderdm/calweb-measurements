package nl.bori.calweb.services.measurementsservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bepaling_riza_excel")
public class BepalingRizaExcel implements java.io.Serializable {

	private static final long serialVersionUID = 1874508722809513883L;

	@Id
	@Column(name = "id_bepaling", unique = true, nullable = false)
	private int idBepaling;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bepaling", unique = true, nullable = false, insertable = false, updatable = false)
	private Bepaling bepaling;

	@Column(name = "opmerkingen")
	private String opmerkingen;

	public BepalingRizaExcel() {
	}

	public int getIdBepaling() {
		return this.idBepaling;
	}

	public void setIdBepaling(int idBepaling) {
		this.idBepaling = idBepaling;
	}

	public Bepaling getBepaling() {
		return this.bepaling;
	}

	public void setBepaling(Bepaling bepaling) {
		this.bepaling = bepaling;
	}

	public String getOpmerkingen() {
		return this.opmerkingen;
	}

	public void setOpmerkingen(String opmerkingen) {
		this.opmerkingen = opmerkingen;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idBepaling;
		result = prime * result
				+ ((opmerkingen == null) ? 0 : opmerkingen.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BepalingRizaExcel other = (BepalingRizaExcel) obj;
		if (idBepaling != other.idBepaling)
			return false;
		if (opmerkingen == null) {
			if (other.opmerkingen != null)
				return false;
		} else if (!opmerkingen.equals(other.opmerkingen))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BepalingRizaExcel [idBepaling=" + idBepaling + ", opmerkingen="
				+ opmerkingen + "]";
	}

}

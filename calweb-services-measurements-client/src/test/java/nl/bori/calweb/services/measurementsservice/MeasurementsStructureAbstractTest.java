/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.DefaultMetadataHandler;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.hibernate.HibernateException;
import org.hibernate.impl.SessionImpl;
import org.junit.AfterClass;
import org.junit.BeforeClass;


/**
 * @author marnix
 *
 */
public abstract class MeasurementsStructureAbstractTest {

	protected static String persistenceUnitName = "stcmetingenPU";
	protected static String persistenceUnitTestName = persistenceUnitName + "-test";

	
	protected Logger logger = Logger.getLogger(MeasurementsStructureAbstractTest.class);
	
	private static EntityManagerFactory emf;	
	private static EntityManager em;
	
	/* 
	 * Database attributes
	 */
	private static IDataSet dataset;
	private static IDatabaseConnection jdbcConnection;
	
	@BeforeClass
	public static void classSetUp() throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException {

		emf = Persistence.createEntityManagerFactory(persistenceUnitTestName);
		setEntityManager(emf.createEntityManager());
		
		/* Initialise the attributes jdbcConnection and dataSet */
		MeasurementsStructureAbstractTest.prepareDatabaseInformation();

	}
	
	@AfterClass
	public static void classCleanUp() throws SQLException {
		
		if (em != null && em.isOpen()) em.close();
		if (emf != null && emf.isOpen()) emf.close();
		
	}
	
	/**
	 * Prepare a jdbc connection to the database that can be used to manipulate the content
	 * of the database for the tests.  The content of the database is stored in an xml file.
	 * 
	 * The connection is created using an entitymanager
	 * 
	 * @throws DatabaseUnitException if a jdbc connection cannot be created.
	 * @throws HibernateException  
	 * @throws FileNotFoundException if the file with the content of the database cannot be found.
	 */
	private static void prepareDatabaseInformation() throws HibernateException, DatabaseUnitException, FileNotFoundException {

		/*
		 * Creeer een jdbc verbinding naar de inmem database uit de entitymanager.
		 */
		jdbcConnection = new DatabaseConnection(((SessionImpl)(getEntityManager().getDelegate())).connection()); 
		
		/*
		 * data type factory instellen
		 */
		DatabaseConfig config = jdbcConnection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());
		config.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new DefaultMetadataHandler());
		config.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, false);

		dataset = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/database/stcmetingen.content.xml"));

	}
	
	/**
	 * Verwijder de gegevens die ingevoerd zijn bij het begin van een test.
	 * @throws ClassNotFoundException 
	 * @throws DatabaseUnitException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	protected void prepareDatabase() throws ClassNotFoundException, DatabaseUnitException, SQLException, FileNotFoundException {
		
	    Connection c = ((SessionImpl) getEntityManager().getDelegate()).connection();
	    Statement s = c.createStatement();
	    s.execute("SET REFERENTIAL_INTEGRITY FALSE");

		DatabaseOperation.CLEAN_INSERT.execute(jdbcConnection, dataset);
		
	    s.execute("SET REFERENTIAL_INTEGRITY TRUE");
	    s.close();

	}

	/**
	 * @param em the em to set
	 */
	private static void setEntityManager(EntityManager em) {
		MeasurementsStructureAbstractTest.em = em;
	}

	/**
	 * @return the em
	 */
	protected static EntityManager getEntityManager() {
		return em;
	}



	
}

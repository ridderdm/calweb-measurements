/**
 * $Id:$
 */
package nl.bori.calweb.services.measurementsservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;

import nl.bori.calweb.services.measurementsservice.model.Meetgegevens;
import nl.bori.calweb.services.measurementsservice.model.Monster;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author marnix
 *
 */
public class MeasurementsMeetgegevensStructureTest extends
		MeasurementsStructureAbstractTest {

	private static final int allMeetgegevens = 7;
	
	@Before
	public void setUp() throws FileNotFoundException, ClassNotFoundException,
			DatabaseUnitException, SQLException {

		prepareDatabase();
	}
	
	@Test
	/*
	 * Test of we alle meetgegevens terugkrijgen bij een query
	 */
	public void testAllMonsters() {

		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens");
		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();

		assertEquals(allMeetgegevens, monsters.size());

	}
	
	@Test
	public void testMeetgegvensStructureBrandweer() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 2");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensBrandweer());
		}
	}
	
	@Test
	public void testMeetgegvensStructureRikilt() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 1");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensRikilt());
		}
	}
	
	@Test
	public void testMeetgegvensStructureRizaExcel() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 3");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensRizaExcel());
		}
	}
	
	@Test
	public void testMeetgegvensStructureVWA() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 4");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensVwa());
			assertNull(meetgegeven.getMeetgegevensBrandweer());
		}
	}
	
	@Test
	public void testMeetgegvensStructureRIVMLIMS() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 7");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensRivmLims());
		}
	}
	
	@Test
	public void testMeetgegvensStructureRIVM() {
		
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 6");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensRivm());
		}
	}
	
	
	@Test
	public void testMeetgegvensStructureRIZAXML() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
						+ " where "
						+ " meetgegevens.id = 5");

		@SuppressWarnings("unchecked")
		List<Meetgegevens> meetgegevens = query.getResultList();
		
		assertEquals(1, meetgegevens.size());
		
		for (Meetgegevens meetgegeven : meetgegevens) {
			assertNotNull(meetgegeven.getMeetgegevensRizaXml());
		}
	}
	
	@Test
	public void testMeetgegevens2Childs() {
		Query query = getEntityManager().createQuery(
				"select distinct meetgegevens from Meetgegevens meetgegevens "
				+ " where meetgegevens.id = 1");

		Meetgegevens meetgegevens = (Meetgegevens) query.getSingleResult();
		
		assertEquals(1, meetgegevens.getId().intValue());
		
		/* Afzender */
		assertNotNull(meetgegevens.getAfzender());
		assertEquals("RIKILT regulier", meetgegevens.getAfzender().getNaam());
		
		/* Bestand */
		assertNotNull(meetgegevens.getBestand());
		assertEquals("received-report-Wed Jun 02 14:52:44 CEST 2010-85323bc2-3dc5-4cad-91b8-e17548f59110.xml", 
				meetgegevens.getBestand().getNaam());
		
		/* Monster */
		assertEquals(3, meetgegevens.getMonsters().size());
		
	}
	
}

/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;

import nl.bori.calweb.services.measurementsservice.model.Bepaling;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;

/**
 * @author marnix
 *
 */
public class MeasurementsBepalingStructureTest extends
		MeasurementsStructureAbstractTest {
	
	private static final int allBepalingen = 68;

	@Before
	public void setUp() throws FileNotFoundException, ClassNotFoundException,
			DatabaseUnitException, SQLException {

		prepareDatabase();
	}

	@Test
	/*
	 * Test of we alle monsters terugkrijgen bij een query
	 */
	public void testAllBepalingen() {

		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling");
		@SuppressWarnings("unchecked")
		List<Bepaling> bepaling = query.getResultList();

		assertEquals(allBepalingen, bepaling.size());
	}
	

	@Test
	public void testBepalingStructureBrandweer() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 2");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(13, bepalingen.size());

		/* er is geen bepaling_brandweer */

	}
	
	@Test
	public void testBepalingStructureRikilt() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 1");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(5, bepalingen.size());
		
		for (Bepaling bepaling : bepalingen) {
			assertNotNull(bepaling.getBepalingRikilt());
			assertNotNull(bepaling.getMeetwaarde());
		}

	}
	
	//@Test
	public void testBepalingStructureRIVM() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 6");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(3, bepalingen.size());
		
		for (Bepaling bepaling : bepalingen) {
			assertNotNull(bepaling.getBepalingRivm());
			assertNotNull(bepaling.getMonster());
		}
	}

	//@Test
	public void testBepalingStructureRIVMLIMS() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 7");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(4, bepalingen.size());
		
		for (Bepaling bepaling : bepalingen) {
			assertNotNull(bepaling.getBepalingRivm());
			assertNotNull(bepaling.getMonster());
		}
	}
	
	@Test
	public void testBepalingStructureRizaExcel() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 3");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(28, bepalingen.size());
		
		for (Bepaling bepaling : bepalingen) {
			assertNotNull(bepaling.getBepalingRizaExcel());
			assertNotNull(bepaling.getMonster());
		}
	}
	

	public void testBepalingStructureRizaXML() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 3");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(28, bepalingen.size());
		
		for (Bepaling bepaling : bepalingen) {
			assertNotNull(bepaling.getBepalingRizaXml());
			assertNotNull(bepaling.getMonster());
		}
	}
	
	@Test
	public void testBepalingStructureVWAL() {
		Query query = getEntityManager().createQuery(
				"select distinct bepaling from " + " Bepaling bepaling, "
						+ " IN(bepaling.monster) monster, " 
						+ " IN(monster.meetgegevens) meetgegevens "
						+ " where "
						+ " meetgegevens.id = 4");

		@SuppressWarnings("unchecked")
		List<Bepaling> bepalingen = query.getResultList();
		
		assertEquals(22, bepalingen.size());
		
		for (Bepaling bepaling : bepalingen) {
			assertNotNull(bepaling.getBepalingVwa());
			assertNotNull(bepaling.getMonster());
		}
	}

}

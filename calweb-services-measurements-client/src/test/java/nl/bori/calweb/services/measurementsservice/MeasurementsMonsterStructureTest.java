/**
 * $Id:$
 */
package nl.bori.calweb.services.measurementsservice;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.Query;

import nl.bori.calweb.services.measurementsservice.model.Monster;

import org.dbunit.DatabaseUnitException;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author marnix
 * 
 */
public class MeasurementsMonsterStructureTest extends
		MeasurementsStructureAbstractTest {

	private static final int allMonsters = 51;
	
	@Before
	public void setUp() throws FileNotFoundException, ClassNotFoundException,
			DatabaseUnitException, SQLException {

		prepareDatabase();
	}

	@Test
	/*
	 * Test of we alle monsters terugkrijgen bij een query
	 */
	public void testAllMonsters() {

		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster");
		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();

		assertEquals(allMonsters, monsters.size());

	}
	
	@Test
	/*
	 * De zending met id=3 bevat metingen van RIZA in Excel formaat.
	 */
	public void testMonsterStructureBrandweer() {
		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 2");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(13, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterBrandweer());
		}

	}
	
	@Test
	public void testMonsterStructureRIKILT() {
		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 1");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(3, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterRikilt());
		}

	}



	@Test
	public void testMonsterStructureRIVM() {
		

		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 6");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(3, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterRivm());
		}


	}
	


	@Test
	public void testMonsterStructureRIVMLIMS() {
		

		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 7");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(4, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterRivmLims());
		}

	}



	@Test
	/*
	 * De zending met id=3 bevat metingen van RIZA in Excel formaat.
	 */
	public void testMonsterStructureRIZAExcel() {
		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 3");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(4, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterRizaExcel());
		}

	}
	
	@Test
	public void testMonsterStructureRIZAXML() {

		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 5");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(2, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterRizaXml());
		}
		

	}
	


	@Test
	public void testMonsterStructureVWA() {
		Query query = getEntityManager().createQuery(
				"select distinct monster from " + " Monster monster, "
						+ " IN(monster.meetgegevens) meetgegevens " + " where "
						+ " meetgegevens.id = 4");

		@SuppressWarnings("unchecked")
		List<Monster> monsters = query.getResultList();
		
		assertEquals(22, monsters.size());
		
		for (Monster monster : monsters) {
			assertNotNull(monster.getMonsterVwa());
		}

	}

	
}

/**
 * $Id$
 */
package nl.bori.calweb.test.exporter.measurements;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;

/**
 * @author ridderdm
 *
 * Klasse waarmee de database met metingen geexporteerd kan worden naar een XML bestand dat door
 * DBUnit gebruikt kan worden voor de initialisatie van de tabellen zodat testen deterministisch
 * uitgevoerd kunenn worden.
 * 
 * Deze klasse moet handmatig uitgevoerd worden na een wijziging van de tabellen of inhouden van de tabellen.
 * 
 * Bij uitvoering van deze klasse kan een melding worden gegeven dat het schema "stcmetingen" niet bestaat.
 * Dat lijkt een bug in de MySQL driver klasse te zijn, volgens commentaar in de broncode van DBUnit. 
 */
public class MeasurementsExporter {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 * @throws DatabaseUnitException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws ClassNotFoundException, SQLException, DatabaseUnitException, FileNotFoundException, IOException {
		
		int tableNameColumn = 3;
		String liquibaseTableNamePrefix = "DATABASECHANGELOG";
		
		@SuppressWarnings("unused")
		Class driverClass = Class.forName("com.mysql.jdbc.Driver");
		Connection jdbcConnection = DriverManager.getConnection("jdbc:mysql://localhost/stcmetingen", "calweb", "calweb!");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection, "stcmetingen");
		
		/*
		 * data type factory instellen
		 */
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		// TODO config.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());
		
		/*
		 * Exporteer alle tabellen
		 */
		
		/*
		 * Wat hieronder staat en wat van de DBUnit website afkomstig is, werkt niet.
		 * Je krijgt een foutmelding over de liquibase tabellen.
		 * 
		 * TODO testen of de totale export wel werkt als je een metadatahandler toevoegt, echter,
		 * niet alle tabellen moeten geexporteerd worden. 
		 */
		/*
		IDataSet dataSet = connection.createDataSet();
		FlatXmlDataSet.write(dataSet, new FileOutputStream("src/test/resources/database/stcmetingen.content.xml"));
		*/
		
		DatabaseMetaData metadata = jdbcConnection.getMetaData();
		ResultSet tables = metadata.getTables(null, null, "%", null);
		
		/*
		 * TODO
		 * 
		 * Je kunt niet zomaar alle tabellen exporteren omdat dan bij de invoer problemen optreden met
		 * de onderlinge afhankelijkheid van de tabellen: constraints.  Je tabellen moeten daarom in een
		 * volgorde geexporteert worden bij de tabellen zonder afhankelijkheden als eerste geexporteerd worden, 
		 * waarna de tabellen die ervan afhankelijk zijn enz.
		 * 
		 * Dus : eerst locatie, dan monster, dan bepaling.
		 */
		
		QueryDataSet dataSet = new QueryDataSet(connection);
		while (tables.next()) {
			if (tables.getString(tableNameColumn).startsWith(liquibaseTableNamePrefix) ||
					tables.getString(tableNameColumn).equals("gemcentroide") ||
					tables.getString(tableNameColumn).contains("compartimentInfo") ||
					tables.getString(tableNameColumn).equals("matrix")) {
				System.out.println("Skip tabel " + tables.getString(tableNameColumn));
				continue;
			}
			System.out.println("Exporteer tabel " + tables.getString(tableNameColumn));
			dataSet.addTable(tables.getString(tableNameColumn));			
		}
		
		FlatXmlDataSet.write(dataSet, new FileOutputStream("src/test/resources/database/stcmetingen.content.xml"));

		
	}

}

/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice.beans;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.ext.mysql.MySqlMetadataHandler;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * @author marnix
 *
 */
public abstract class MeasurementsServiceBeanAbstractTest {

	protected static String persistenceUnitName = "stcmetingenPU";
	protected static String persistenceUnitTestName = persistenceUnitName + "-test";
	protected static EntityManagerFactory emf;
	protected Logger logger = Logger.getLogger(MeasurementsServiceBeanAbstractTest.class);

	
	
	@BeforeClass
	public static void classSetUp() throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException {
		emf = Persistence.createEntityManagerFactory(persistenceUnitTestName);
		
		/* vul de database met metingen zodat we weten waartegen we testen */
		MeasurementsServiceBeanAbstractTest.fillDatabase();
	}
	
	@AfterClass
	public static void classCleanUp() throws FileNotFoundException, ClassNotFoundException, DatabaseUnitException, SQLException {
		if (emf != null && emf.isOpen()) emf.close();

		MeasurementsServiceBeanAbstractTest.cleanDatabase();
	}
	
	/**
	 * Vul de database met gegevens
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 * @throws DatabaseUnitException 
	 * @throws IOException 
	 */
	protected static void fillDatabase() throws ClassNotFoundException, SQLException, DatabaseUnitException, IOException {
	
		/* maak verbinding met de database */
		@SuppressWarnings({ "unused", "rawtypes" })
		Class driverClass = Class.forName("com.mysql.jdbc.Driver");
		Connection jdbcConnection = DriverManager.getConnection("jdbc:mysql://localhost/stcmetingen", "calweb", "calweb!");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection, "stcmetingen");
		
		/*
		 * data type factory instellen
		 */
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		config.setProperty(DatabaseConfig.PROPERTY_METADATA_HANDLER, new MySqlMetadataHandler());
		config.setProperty(DatabaseConfig.FEATURE_CASE_SENSITIVE_TABLE_NAMES, true);

		IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/database/stcmetingen.content.xml"));
		
		try {
			DatabaseOperation.INSERT.execute(connection, dataSet);
		}
		finally {
			connection.close();
		}

	}
	
	/**
	 * Verwijder de gegevens die ingevoerd zijn bij het begin van een test.
	 * @throws ClassNotFoundException 
	 * @throws DatabaseUnitException 
	 * @throws SQLException 
	 * @throws FileNotFoundException 
	 */
	protected static void cleanDatabase() throws ClassNotFoundException, DatabaseUnitException, SQLException, FileNotFoundException {

		/* maak verbinding met de database */
		@SuppressWarnings({ "unused", "rawtypes" })
		Class driverClass = Class.forName("com.mysql.jdbc.Driver");
		Connection jdbcConnection = DriverManager.getConnection("jdbc:mysql://localhost/stcmetingen", "root", "");
		IDatabaseConnection connection = new DatabaseConnection(jdbcConnection, "stcmetingen");
		
		/*
		 * data type factory instellen
		 */
		DatabaseConfig config = connection.getConfig();
		config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
		
		IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/database/stcmetingen.content.xml"));
		
		try {
			/*
			 * Verwijder alle gegevens uit de tabellen die genoemd worden in de dataSet.
			 * Alleen DELETE verwijdert de gegevens die genoemd zijn in de dataSet.
			 */
			DatabaseOperation.DELETE_ALL.execute(connection, dataSet);
		}
		finally {
			connection.close();
		}
		
	}


	
}

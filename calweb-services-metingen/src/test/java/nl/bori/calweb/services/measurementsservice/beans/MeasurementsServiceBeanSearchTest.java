/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice.beans;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import nl.bori.calweb.services.measurementsservice.MeasurementSearchControls;
import nl.bori.calweb.services.measurementsservice.MeasurementsSearchControls;
import nl.bori.calweb.services.measurementsservice.Meetgegevens;
import nl.bori.calweb.services.measurementsservice.SampleSearchControls;
import nl.bori.calweb.services.measurementsservice.beans.MeasurementsServiceBean;
import nl.bori.calweb.services.retrieval.RetrievalException;

import static org.junit.Assert.*;

import org.dbunit.DatabaseUnitException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.MutableDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.stvconsultants.easygloss.StandardGloss;
import com.stvconsultants.easygloss.footnotes.Footnote;

/**
 * @author ridderdm
 *
 */
public class MeasurementsServiceBeanSearchTest extends MeasurementsServiceBeanAbstractTest {


	private EntityManager em = null;
	private MeasurementsServiceBean bean;
	
	private static int allMeasurementsCount = 4;
	
	public MeasurementsServiceBeanSearchTest() {
		
	}
	
		
	@Before
	public void setUp() {
		
		logger.info("setUp() van de entitymanager(factory) en de bean.");

		em = MeasurementsServiceBeanAbstractTest.emf.createEntityManager();
		
		bean = new MeasurementsServiceBean();
		
		StandardGloss gloss = new StandardGloss(
				new Footnote(PersistenceContext.class).with("unitName", MeasurementsServiceBeanAbstractTest.persistenceUnitName ), em);
		gloss.apply(bean);
	}
	
	@After
	public void cleanUp() {
		logger.info("cleanUp() van de testen");
		if (em != null && em.isOpen()) em.close();

	}
	
	@Test
	public void searchWithNoControls() throws RetrievalException {
		
		/*
		 * De ingevoerde metingen zijn van begin juni 2010 dus mogen nooit gevonden
		 * worden.
		 */
		
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("0");
		List<Meetgegevens> resultList = bean.search(searchControls);
		assertEquals(0, resultList.size());	
		
		searchControls.setProjectId("254");
		resultList = bean.search(searchControls);
		assertEquals(0, resultList.size());	
		
	}
	
	
	/*
	 * 
	 * De test op zoeken met monstername gegevens. 
	 * 
	 * De bean gebruikt defaults voor het zoeken op tijdstip van upload, om deze test
	 * op het zoeken met monstergegevens goed uit te voeren, moeten we het zoeken op
	 * de upload tijd "uitschakelen" door een zeer grote periode in te voeren waarin
	 * alle opgestuurde metingen vallen.  
	 */
	@Test 
	public void searchWithSampleDateLimits() throws RetrievalException {

		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("254");
		
		MutableDateTime nextMonth = new MutableDateTime(); // nextMonth is today
		nextMonth.addMonths(1);

		searchControls.setDateOfUploadEnd(nextMonth.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);

		/*
		 * Zoek met begin datum voor monstername
		 */
		
		SampleSearchControls monsterControls = new SampleSearchControls();
		MutableDateTime year2008 = new MutableDateTime();
		year2008.setDayOfMonth(1);
		year2008.setMonthOfYear(DateTimeConstants.JANUARY);
		year2008.setYear(2008);
		monsterControls.setStartDate(year2008.toDateTime());
		searchControls.setSampleSearchControls(monsterControls);
		
		List<Meetgegevens> resultList = bean.search(searchControls);
		assertEquals(2, resultList.size());
		
		/*
		 * Zoek met eind datum voor monstername
		 */
		
		MutableDateTime year2009 = new MutableDateTime();
		year2009.setDayOfMonth(1);
		year2009.setMonthOfYear(DateTimeConstants.JANUARY);
		year2009.setYear(2009);
		monsterControls.setEndDate(year2009.toDateTime());
		searchControls.setSampleSearchControls(monsterControls);
		
		resultList = bean.search(searchControls);
		assertEquals(2, resultList.size());	

	}
	
	/*
	 * 
	 * De test op zoeken met bepaling gegevens. 
	 * 
	 * De bean gebruikt defaults voor het zoeken op tijdstip van upload, om deze test
	 * op het zoeken met monstergegevens goed uit te voeren, moeten we het zoeken op
	 * de upload tijd "uitschakelen" door een zeer grote periode in te voeren waarin
	 * alle opgestuurde metingen vallen.  
	 */
	@Test 
	public void searchWithMeasurementDateLimits() throws RetrievalException {

		/*
		 * Maak een control voor zoeken op bepaling en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("254");
		
		MutableDateTime nextMonth = new MutableDateTime(); // nextMonth is today
		nextMonth.addMonths(1);
		
		searchControls.setDateOfUploadEnd(nextMonth.toDateTime());
		/*
		Calendar middleAges = (Calendar) today.clone();
		middleAges.add(Calendar.YEAR, -500);
		*/
		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);
		
		SampleSearchControls sampleControls = new SampleSearchControls();
		
		MeasurementSearchControls bepalingControls = new MeasurementSearchControls();
		
		/*
		 * Test wat er gebeurd als je geen controls opgeeft
		 */
		sampleControls.setMeasurementSearchControls(bepalingControls);
		searchControls.setSampleSearchControls(sampleControls);
		
		List<Meetgegevens> resultList = bean.search(searchControls);
		assertEquals(allMeasurementsCount, resultList.size());	
		
		/*
		 * Nu een einddatum opgeven
		 */
		
		MutableDateTime endDate = new MutableDateTime();
		endDate.setYear(2007);
		endDate.setMonthOfYear(DateTimeConstants.DECEMBER);
		endDate.setDayOfMonth(31);
		
		
		bepalingControls.setEndDate(endDate.toDateTime());
		
		sampleControls.setMeasurementSearchControls(bepalingControls);
		searchControls.setSampleSearchControls(sampleControls);		
		
		resultList = bean.search(searchControls);
		assertEquals(2, resultList.size());
		
		/*
		 * Nu een begindatum toevoegen
		 */
		MutableDateTime beginDate = new MutableDateTime();
		beginDate.setYear(2007);
		beginDate.setMonthOfYear(DateTimeConstants.JANUARY);
		beginDate.setDayOfMonth(1);
		bepalingControls.setStartDate(beginDate.toDateTime());
		
		sampleControls.setMeasurementSearchControls(bepalingControls);
		searchControls.setSampleSearchControls(sampleControls);		
		
		resultList = bean.search(searchControls);
		assertEquals(1, resultList.size());		
		
	}

	
	
	@Test
	public void searchWithUploadDateLimits() throws RetrievalException {
		
		/*
		 * Alle metingen vallen onder project 254
		 */
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("254");
		
		/*
		 * zet nu een startdatum waardoor een aantal metingen buiten de selectie vallen 
		 */
		MutableDateTime mutableStartDate = new MutableDateTime();
		mutableStartDate.setYear(2010);
		mutableStartDate.setMonthOfYear(DateTimeConstants.JUNE);
		mutableStartDate.setDayOfMonth(1);
		
		searchControls.setDateOfUploadStart(mutableStartDate.toDateTime());
		
		List<Meetgegevens> resultList = bean.search(searchControls);
		assertEquals(3, resultList.size());
		
		/*
		 * en zet nu een startdatum waardoor alle metingen binnen de selectie vallen
		 */
		mutableStartDate.setMonthOfYear(DateTimeConstants.MAY);
		searchControls.setDateOfUploadStart(mutableStartDate.toDateTime());
		
		resultList = bean.search(searchControls);
		assertEquals(allMeasurementsCount, resultList.size());

		/*
		 * Zet nu een einddatum waardoor alle metingen binnen de selectie vallen
		 * De startdatum is nog steeds maart uit de vorige test
		 */

		MutableDateTime endDate = new MutableDateTime();
		endDate.setMonthOfYear(DateTimeConstants.JULY);
		endDate.setYear(2010);
		endDate.setDayOfMonth(1);
		searchControls.setDateOfUploadEnd(endDate.toDateTime());
		
		resultList = bean.search(searchControls);
		assertEquals(allMeasurementsCount, resultList.size());

		/*
		 * Wijzig nu de startdatum waardoor metingen buiten de selectie vallen.
		 * De startdatum is leeg
		 */
		searchControls.setDateOfUploadStart(null);
		
		resultList = bean.search(searchControls);
		assertEquals(3, resultList.size());


		searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("254");

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);
		
		resultList = bean.search(searchControls);
		
		assertEquals(allMeasurementsCount, resultList.size());

	}
	
	@Test
	public void getAvailableNuclides() {
		
		List<String> resultList = bean.getAvailableNuclides();
		assertEquals(13, resultList.size());
		
	}
	
	@Test
	public void searchWithNuclides() throws RetrievalException {
	
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("254");
		
		/*
		 * Maak een control voor zoeken op monster en zorg ervoor dat alle
		 * testmetingen binnen de per default doorzochte periode van uploads
		 * vallen.
		 */

		MutableDateTime today = new MutableDateTime();
		today.addMonths(1);
		searchControls.setDateOfUploadEnd(today.toDateTime());

		DateTime middleAges = (new DateTime()).minusYears(400);
		searchControls.setDateOfUploadStart(middleAges);


		List<String> list = new ArrayList<String>();
		list.add("Cs-137");
		MeasurementSearchControls bepalingControls = new MeasurementSearchControls();
		bepalingControls.setNuclide(list);
		
		SampleSearchControls sampleControls = new SampleSearchControls();
		sampleControls.setMeasurementSearchControls(bepalingControls);
		
		searchControls.setSampleSearchControls(sampleControls);
		
		List<Meetgegevens> resultList = bean.search(searchControls);
		assertEquals(2, resultList.size());

		/*
		 * nuclide toevoegen
		 */
		list.add("I-131");
		bepalingControls.setNuclide(list);
		
		sampleControls = new SampleSearchControls();
		sampleControls.setMeasurementSearchControls(bepalingControls);
		
		searchControls.setSampleSearchControls(sampleControls);
		
		resultList = bean.search(searchControls);
		assertEquals(3, resultList.size());
		
	}
	
	@Test
	public void testGetByIdentifier() throws RetrievalException {
		
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setIdentifier("5");
		
		List<Meetgegevens> list = bean.search(searchControls);
		
		assertEquals(1, list.size());
		assertEquals(new Integer(5), list.get(0).getId());
		
		
	}
	
	@Test
	public void testWithEmptyDatabase() throws FileNotFoundException, ClassNotFoundException, DatabaseUnitException, SQLException, RetrievalException {
		
		MeasurementsServiceBeanAbstractTest.cleanDatabase();
		
		MeasurementsSearchControls searchControls = new MeasurementsSearchControls();
		searchControls.setProjectId("254");
		
		assertEquals(0, bean.getAvailableNuclides().size());
		
		assertEquals(0, bean.search(searchControls).size());
		
	}
	

	
	
}

<#-- // Fields -->

<#foreach field in pojo.getAllPropertiesIterator()><#if pojo.getMetaAttribAsBool(field, "gen-property", true)> <#if pojo.hasMetaAttribute(field, "field-description")>    /**
     ${pojo.getFieldJavaDoc(field, 0)}
     */
 </#if>    
 	<#assign typename>${pojo.getJavaTypeName(field, jdk5)}</#assign>
 	
 ${pojo.getFieldModifiers(field)} ${typename?replace("Set", "List", 'f')} ${field.name}<#if pojo.hasFieldInitializor(field, jdk5)> = ${pojo.getFieldInitialization(field, jdk5)?replace("HashSet", "ArrayList", 'f')}</#if>;
</#if>
</#foreach>

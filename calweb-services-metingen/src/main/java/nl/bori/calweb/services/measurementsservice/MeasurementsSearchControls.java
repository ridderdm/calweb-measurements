/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import nl.bori.calweb.services.retrieval.SearchControls;

/**
 * @author ridderdm
 * 
 *         Klasse die zoekt op attributen uit de klasse Meetgegevens.
 * 
 *         De MeasurementService gebruikt configureerbare defaults indien een
 *         parameter niet is gebruikt.
 * 
 */
public class MeasurementsSearchControls extends SearchControls {

	private static final long serialVersionUID = 8995612906697547200L;

	private static final Logger logger = Logger
			.getLogger(MeasurementsSearchControls.class);

	/**
	 * Zoek op datumOntvangst
	 */
	private DateTime dateOfUploadStart = null;
	private DateTime dateOfUploadEnd = null;

	/**
	 * Zoek op afzender van de gegevens
	 */
	private String afzender;

	/**
	 * Zoekparamaters voor het Monster
	 */
	private SampleSearchControls sampleSearchControls = new SampleSearchControls();

	public MeasurementsSearchControls() {
		super();
	}

	/**
	 * @param dateOfUploadStart
	 * @param dateOfUploadEnd
	 * @param afzender
	 * @param sampleSearchControls
	 */
	public MeasurementsSearchControls(DateTime dateOfUploadStart,
			DateTime dateOfUploadEnd, String afzender,
			SampleSearchControls sampleSearchControls) {
		super();
		this.dateOfUploadStart = dateOfUploadStart;
		this.dateOfUploadEnd = dateOfUploadEnd;
		this.afzender = afzender;
		this.sampleSearchControls = sampleSearchControls;
	}

	/**
	 * @return the dateOfUpload
	 */
	public DateTime getDateOfUploadStart() {
		return dateOfUploadStart;
	}

	/**
	 * @param dateOfUpload
	 *            the dateOfUpload to set
	 */
	public void setDateOfUploadStart(DateTime dateOfUploadStart) {
		if (logger.isDebugEnabled() && dateOfUploadStart != null) {
			logger.debug("setDateOfUploadStart(" + dateOfUploadStart + ")");
		}
		this.dateOfUploadStart = dateOfUploadStart;
	}

	/**
	 * @return the dateOfUploadEnd
	 */
	public DateTime getDateOfUploadEnd() {
		return dateOfUploadEnd;
	}

	/**
	 * @param dateOfUploadEnd
	 *            the dateOfUploadEnd to set
	 */
	public void setDateOfUploadEnd(DateTime dateOfUploadEnd) {
		if (logger.isDebugEnabled() && dateOfUploadEnd != null) {
			logger.debug("setDateOfUploadEnd(" + dateOfUploadEnd + ")");
		}
		this.dateOfUploadEnd = dateOfUploadEnd;
	}

	/**
	 * @return the afzender
	 */
	public String getAfzender() {
		return afzender;
	}

	/**
	 * @param afzender
	 *            the afzender to set
	 */
	public void setAfzender(String afzender) {
		this.afzender = afzender;
	}

	@Override
	public void setProjectId(String projectId) {
		/*
		 * Deze klasse wordt gebruikt in het huidige CalWeb waarin de status van
		 * CalWeb wordt aangeduid met een integer. Deze functie overschrijft
		 * daarom de super-functie en zorgt ervoor dat de ontvangen string
		 * altijd als int gezien kan worden en zo niet, maakt er een 0 van.
		 */
		Integer statusId = 0;
		try {
			statusId = Integer.valueOf(projectId);
		} catch (NumberFormatException e) {
			logger.error("Received projectId '"
					+ projectId
					+ "' cannot be represented as an integer which is necessary "
					+ "to use it as a CalWeb status identifier.  Setting status id to 0");
		} finally {
			super.setProjectId(statusId.toString());
		}
	}

	public void setCalwebStatusId(int statusId) {
		super.setProjectId(new Integer(statusId).toString());
	}

	public int getCalwebStatusId() {
		return Integer.valueOf(super.getProjectId()).intValue();
	}

	/**
	 * @return the sampleSearchControls
	 */
	public SampleSearchControls getSampleSearchControls() {
		return sampleSearchControls;
	}

	/**
	 * @param sampleSearchControls
	 *            the sampleSearchControls to set
	 */
	public void setSampleSearchControls(
			SampleSearchControls sampleSearchControls) {
		this.sampleSearchControls = sampleSearchControls;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((afzender == null) ? 0 : afzender.hashCode());
		result = prime * result
				+ ((dateOfUploadEnd == null) ? 0 : dateOfUploadEnd.hashCode());
		result = prime
				* result
				+ ((dateOfUploadStart == null) ? 0 : dateOfUploadStart
						.hashCode());
		result = prime
				* result
				+ ((sampleSearchControls == null) ? 0 : sampleSearchControls
						.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof MeasurementsSearchControls)) {
			return false;
		}
		MeasurementsSearchControls other = (MeasurementsSearchControls) obj;
		if (afzender == null) {
			if (other.afzender != null) {
				return false;
			}
		} else if (!afzender.equals(other.afzender)) {
			return false;
		}
		if (dateOfUploadEnd == null) {
			if (other.dateOfUploadEnd != null) {
				return false;
			}
		} else if (!dateOfUploadEnd.equals(other.dateOfUploadEnd)) {
			return false;
		}
		if (dateOfUploadStart == null) {
			if (other.dateOfUploadStart != null) {
				return false;
			}
		} else if (!dateOfUploadStart.equals(other.dateOfUploadStart)) {
			return false;
		}
		if (sampleSearchControls == null) {
			if (other.sampleSearchControls != null) {
				return false;
			}
		} else if (!sampleSearchControls.equals(other.sampleSearchControls)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeasurementsSearchControls [dateOfUploadStart="
				+ dateOfUploadStart + ", dateOfUploadEnd=" + dateOfUploadEnd
				+ ", afzender=" + afzender + ", sampleSearchControls="
				+ sampleSearchControls + ", toString()=" + super.toString()
				+ "]";
	}



}

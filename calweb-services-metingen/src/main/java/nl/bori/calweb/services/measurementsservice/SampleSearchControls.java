/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import nl.bori.calweb.services.retrieval.SearchControls;

/**
 * @author ridderdm
 * 
 *         Klasse die zoekt op attributen van de Monster klasse
 */
public class SampleSearchControls extends SearchControls {

	private static final long serialVersionUID = 6098206472720086581L;

	private static final Logger logger = Logger
			.getLogger(SampleSearchControls.class);

	private MeasurementSearchControls measurementSearchControls = new MeasurementSearchControls();

	/**
	 * Monstername begintijd
	 */
	private DateTime startDate = null;

	/**
	 * Monstername eindtijd
	 */
	private DateTime endDate = null;

	public SampleSearchControls() {
		super();
	}

	public SampleSearchControls(DateTime startDate, DateTime endDate) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
	}

	/**
	 * @return the startDate
	 */
	public DateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(DateTime startDate) {
		if (logger.isDebugEnabled() && startDate != null) {
			logger.debug("setStartDate(" + startDate.toString() + ")");
		}
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		if (logger.isDebugEnabled() && endDate != null) {
			logger.debug("setEndDate(" + endDate.toString() + ")");
		}
		this.endDate = endDate;
	}

	/**
	 * @return the measurementSearchControls
	 */
	public MeasurementSearchControls getMeasurementSearchControls() {
		return measurementSearchControls;
	}

	/**
	 * @param measurementSearchControls
	 *            the measurementSearchControls to set
	 */
	public void setMeasurementSearchControls(
			MeasurementSearchControls measurementSearchControls) {
		this.measurementSearchControls = measurementSearchControls;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime
				* result
				+ ((measurementSearchControls == null) ? 0
						: measurementSearchControls.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof SampleSearchControls)) {
			return false;
		}
		SampleSearchControls other = (SampleSearchControls) obj;
		if (endDate == null) {
			if (other.endDate != null) {
				return false;
			}
		} else if (!endDate.equals(other.endDate)) {
			return false;
		}
		if (measurementSearchControls == null) {
			if (other.measurementSearchControls != null) {
				return false;
			}
		} else if (!measurementSearchControls
				.equals(other.measurementSearchControls)) {
			return false;
		}
		if (startDate == null) {
			if (other.startDate != null) {
				return false;
			}
		} else if (!startDate.equals(other.startDate)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SampleSearchControls [measurementSearchControls="
				+ measurementSearchControls + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}

}

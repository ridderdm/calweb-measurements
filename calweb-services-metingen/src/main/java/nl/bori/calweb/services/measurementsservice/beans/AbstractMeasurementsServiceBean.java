/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import nl.bori.calweb.services.measurementsservice.MeasurementsSearchControls;
import nl.bori.calweb.services.measurementsservice.MeasurementsService;
import nl.bori.calweb.services.measurementsservice.Meetgegevens;
import nl.bori.calweb.services.retrieval.RetrievalException;

/**
 * @author ridderdm
 * 
 */
public abstract class AbstractMeasurementsServiceBean implements
		MeasurementsService {

	protected Logger logger = Logger.getLogger(MeasurementsServiceBean.class);

	protected abstract EntityManager getEntityManager();

	private final static String defaultDatetimePattern = "dd-MM-YYYY HH:mm"; // TODO
																				// uit
																				// properties
																				// halen

	/*
	 * TODO kan de toCalendar() methode verwijderd worden TODO bij de toCalendar
	 * wordt nu geen Locale meegegeven, levert dat problemen op?
	 */

	/**
	 * 
	 */
	public AbstractMeasurementsServiceBean() {
		logger.debug("Constructor aangeroepen.");
	}

	/**
	 * @see nl.bori.calweb.services.retrieval.RetrievalService#search(nl.bori.calweb.services.retrieval.SearchControls)
	 * 
	 *      De search() gebruikt voor niet ingevulde parameters van de search
	 *      controls defaults. Deze defaults zijn als volgt:
	 *      <ul>
	 *      <li>datum van ontvangst moet liggen binnen een maand eindigend
	 *      'morgen'</li>
	 *      <li>monstername dag moet eindigen voor 'morgen' en starten na het
	 *      jaar 1500</li>
	 *      <li>meting moet uitgevoerd zijn in een periode die 'morgen' eindigt
	 *      en een maand eerder begint
	 *      <li>nuclides zijn alle bekende nucliden
	 *      </ul>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public List<Meetgegevens> search(MeasurementsSearchControls searchControls)
			throws RetrievalException {

		DateTimeFormatter fmt = DateTimeFormat
				.forPattern(defaultDatetimePattern);

		logger.debug("Search met controls : " + searchControls.toString());

		if (getEntityManager() == null) {
			logger.fatal("EntityManager is null, toegang tot de database is niet mogelijk; kan niet verder gaan.");
			throw new RetrievalException(
					"Toegang tot de 'stcmetingen' database niet mogelijk.");
		}

		if ( !"".equals(searchControls.getIdentifier())  &&  searchControls.getIdentifier() != null) {

			/*
			 * Als een identifier meegegeven is voor de Meetgegevens, zoeken we
			 * direct hiermee.
			 * 
			 * De identifier uit de searchControls is een String, terwijl wij
			 * een integer nodig hebben: conversie
			 */

			if (StringUtils.isNumeric(searchControls.getIdentifier())) {

				Meetgegevens m = getEntityManager().find(Meetgegevens.class,
						new Integer(searchControls.getIdentifier()));

				List<Meetgegevens> resultList = new ArrayList<Meetgegevens>();
				resultList.add(m);
				return resultList;
			}
		}

		Query query = getEntityManager()
				.createQuery(
						"select distinct meetgegevens from Meetgegevens meetgegevens, IN(meetgegevens.monsters) monsters, IN(monsters.bepalings) bepalings where "
								+ "("
								+ "meetgegevens.idCalwebstatus = :projectId "
								+ "and monsters.monsterBegin > :monsternameBegin "
								+ "and monsters.monsterEind < :monsternameEnd "
								+ "and meetgegevens.datumOntvangst between :datumOntvangst_start and :datumOntvangst_end) "
								+ "and ("
									+ "bepalings.nuclide in (:nuclidelist) "
									+ " or bepalings.opgestuurdeNuclide in (:nuclidelist) "
									+ ") "
								+ "and bepalings.bepalingBegin > :bepalingBegin "
								+ "and bepalings.bepalingEind < :bepalingEind "
								+ ")");

		/* het project waartoe de meetgegevens moeten horen */
		logger.debug("Zending moet ingestuurd zijn voor project met identificatie "
				+ (new Integer(searchControls.getProjectId())).intValue());
		query.setParameter("projectId",
				(new Integer(searchControls.getProjectId())).intValue());

		/* periode waarin het monster genomen mag zijn */

		/* begin van de periode van monstername */
		DateTime monsternameStartsAfter = searchControls
				.getSampleSearchControls().getStartDate();
		if (monsternameStartsAfter == null) {
			monsternameStartsAfter = (new DateTime()).minusYears(400)
					.withHourOfDay(0).withMinuteOfHour(0).withMinuteOfHour(0);
		}
		logger.debug("Monster moet genomen zijn na "
				+ fmt.print(monsternameStartsAfter));
		query.setParameter("monsternameBegin",
				monsternameStartsAfter.toCalendar(null), TemporalType.TIMESTAMP);

		/* einde van de periode van monstername */
		DateTime monsternameEndsBefore = searchControls
				.getSampleSearchControls().getEndDate();
		if (monsternameEndsBefore == null) {
			/*
			 * omdat de uren bij deze selectie meetellen, moeten we de dag
			 * component een dagje later zetten.
			 */
			monsternameEndsBefore = (new DateTime()).plusDays(1)
					.withHourOfDay(0).withMinuteOfHour(0);

		}
		logger.debug("Monster moet genomen zijn voor "
				+ fmt.print(monsternameEndsBefore));
		query.setParameter("monsternameEnd",
				monsternameEndsBefore.toCalendar(null), TemporalType.TIMESTAMP);

		/* periode van ontvangst */

		/* eind van de periode */
		DateTime periodOfReceival_End = searchControls.getDateOfUploadEnd();
		if (periodOfReceival_End == null) {

			/*
			 * eind datum wordt huidige datum & tijd waar we een dag bij op
			 * tellen om zeker te weten dat we alles hebben
			 */
			periodOfReceival_End = (new DateTime()).plusDays(1)
					.withHourOfDay(0).withMinuteOfHour(0);

		}
		logger.debug("Zending moet ontvangen zijn voor "
				+ fmt.print(periodOfReceival_End));
		query.setParameter("datumOntvangst_end",
				periodOfReceival_End.toCalendar(null), TemporalType.TIMESTAMP);

		/* begin van de periode */
		DateTime periodOfReceival_Start = searchControls.getDateOfUploadStart();
		if (periodOfReceival_Start == null) {

			/*
			 * als de startdatum leeg is, gebruiken we de eind datum en creeren
			 * we een periode van een maand
			 */

			periodOfReceival_Start = (new DateTime(periodOfReceival_End))
					.minusMonths(1).withHourOfDay(0).withMinuteOfHour(0);

		}

		logger.debug("Zending moet ontvangen zijn na "
				+ fmt.print(periodOfReceival_Start));
		query.setParameter("datumOntvangst_start",
				periodOfReceival_Start.toCalendar(null), TemporalType.TIMESTAMP);

		/* parameters voor de datum en tijd van het uitvoeren van een bepaling */

		/* eind van de periode */
		DateTime periodOfMeasurement_End = searchControls
				.getSampleSearchControls().getMeasurementSearchControls()
				.getEndDate();
		if (periodOfMeasurement_End == null) {

			/*
			 * eind datum wordt huidige datum & tijd waar we een dag bij op
			 * tellen om zeker te weten dat we alles hebben
			 */
			periodOfMeasurement_End = (new DateTime()).plusDays(1)
					.withHourOfDay(0).withMinuteOfHour(0);

		}
		logger.debug("Meting moet uitgevoerd zijn voor "
				+ fmt.print(periodOfMeasurement_End));
		query.setParameter("bepalingEind",
				periodOfMeasurement_End.toCalendar(null),
				TemporalType.TIMESTAMP);

		/* begin van de periode */
		DateTime periodOfMeasurement_Start = searchControls
				.getSampleSearchControls().getMeasurementSearchControls()
				.getStartDate();
		if (periodOfMeasurement_Start == null) {
			periodOfMeasurement_Start = new DateTime(periodOfMeasurement_End)
					.minusYears(400).withHourOfDay(0).withMinuteOfHour(0);
		}
		logger.debug("Meting moet uitgevoerd zijn na "
				+ fmt.print(periodOfMeasurement_Start));
		query.setParameter("bepalingBegin",
				periodOfMeasurement_Start.toCalendar(null),
				TemporalType.TIMESTAMP);

		/*
		 * zoeken naar nucliden
		 */
		if (searchControls.getSampleSearchControls()
				.getMeasurementSearchControls().getNuclide().size() != 0) {
			logger.debug("Nuclide moet er een zijn uit de meegegeven lijst "
					+ searchControls.getSampleSearchControls()
							.getMeasurementSearchControls().getNuclide());
			query.setParameter("nuclidelist", searchControls
					.getSampleSearchControls().getMeasurementSearchControls()
					.getNuclide());
		} else {
			List<String> avNuclides = this.getAvailableNuclides();
			logger.debug("Nuclide moet er een zijn uit de beschikbare lijst "
					+ avNuclides);

			/*
			 * Volgens de JPA 1.0 specificatie moet een lijst die gebruikt wordt
			 * bij het zoeken minimaal 1 element bevatten. Is de lijst leeg, dan
			 * krijg je bij het uitvoeren van de query een foutmelding.
			 * 
			 * Om de foutmelding te vermijden is het volgens diverse bronnen op
			 * internet het eenvoudigst zelf te controleren op null-lijsten. Wij
			 * doen het vrij lomp omdat we niet zelf de query willen verbouwen.
			 * Omdat de lijst geen elementen bevatten is bekend dat de tabellen
			 * geen metingen bevatten. We kunnen dus ieder gewenste tekst in de
			 * lijst stoppen: er komt toch geen meting uit de query.
			 * 
			 * In JPA 2.0 schijnt dit anders te werken.
			 */

			if (avNuclides.size() == 0) {
				avNuclides.add("EenNietBestaandNuclide");
			}

			query.setParameter("nuclidelist", avNuclides);
		}

		/* en nu zoeken */

		List<Meetgegevens> resultList = query.getResultList();
		logger.debug("Aantal gevonden metingen : " + resultList.size());
		return resultList;
	}

	/**
	 * @see
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAvailableNuclides() {

		Query query = getEntityManager()
				.createQuery(
						"select distinct bepaling.nuclide from Bepaling bepaling where nuclide != '' ");
		List<String> result = query.getResultList();
		
		query = getEntityManager().createQuery(
				"select distinct bepaling.opgestuurdeNuclide from Bepaling bepaling where nuclide = '' ");
		result.addAll(query.getResultList());
		
		return result;

	}

}

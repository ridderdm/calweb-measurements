/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice.beans;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.ejb3.annotation.LocalBinding;


/**
 * @author ridderdm
 * 
 */
@Stateless
@LocalBinding(jndiBinding = "ejb/Calweb/measurementsservice")
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MeasurementsServiceBean extends AbstractMeasurementsServiceBean {


	@PersistenceContext(unitName = "stcmetingenPU")
	EntityManager em;

	public MeasurementsServiceBean() {
		logger.debug("Constructor(), em is beschikbaar ? : " + em == null ? "Ja" : "Nee");
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}

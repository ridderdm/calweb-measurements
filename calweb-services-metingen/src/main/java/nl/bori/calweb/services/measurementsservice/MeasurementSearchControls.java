/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import nl.bori.calweb.services.retrieval.SearchControls;

/**
 * @author ridderdm
 * 
 *         Klasse die zoekt op attributen van de klasse Bepaling
 */
public class MeasurementSearchControls extends SearchControls {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4231141637456284119L;

	private static Logger logger = Logger
			.getLogger(MeasurementSearchControls.class);

	/**
	 * Startijd van de meting
	 */
	private DateTime startDate = null;

	/**
	 * Eindtijd van de meting
	 */
	private DateTime endDate = null;

	/**
	 * Meetwaarde
	 */
	private double measurement;

/**
	 * Beperkingen op de meetwaarde.  Bijv "<"
	 */
	private String constraint;

	/**
	 * Het nuclide, dit is zowel het opgesturude nuclide als het CalWeb nuclide
	 */
	private List<String> nuclides = new ArrayList<String>();

	public MeasurementSearchControls() {
		super();
	}

	public MeasurementSearchControls(DateTime startDate, DateTime endDate,
			double measurement, String constraint, List<String> nuclides) {
		super();
		this.startDate = startDate;
		this.endDate = endDate;
		this.measurement = measurement;
		this.constraint = constraint;
		this.nuclides = nuclides;
	}

	/**
	 * @return the startDate
	 */
	public DateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(DateTime startDate) {
		if (logger.isDebugEnabled() && startDate != null) {
			logger.debug("setStartDate(" + startDate + ")");
		}
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate
	 *            the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		if (logger.isDebugEnabled() && endDate != null) {
			logger.debug("SetEndDate(" + endDate + ")");
		}
		this.endDate = endDate;
	}

	/**
	 * @return the measurement
	 */
	public double getMeasurement() {
		return measurement;
	}

	/**
	 * @param measurement
	 *            the measurement to set
	 */
	public void setMeasurement(double measurement) {
		this.measurement = measurement;
	}

	/**
	 * @return the constraint
	 */
	public String getConstraint() {
		return constraint;
	}

	/**
	 * @param constraint
	 *            the constraint to set
	 */
	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

	/**
	 * @param nuclide
	 *            the nuclide to set
	 */
	public void setNuclide(List<String> nuclides) {
		this.nuclides = nuclides;
	}

	/**
	 * @return the nuclide
	 */
	public List<String> getNuclide() {
		return nuclides;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((constraint == null) ? 0 : constraint.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		long temp;
		temp = Double.doubleToLongBits(measurement);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((nuclides == null) ? 0 : nuclides.hashCode());
		result = prime * result
				+ ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof MeasurementSearchControls)) {
			return false;
		}
		MeasurementSearchControls other = (MeasurementSearchControls) obj;
		if (constraint == null) {
			if (other.constraint != null) {
				return false;
			}
		} else if (!constraint.equals(other.constraint)) {
			return false;
		}
		if (endDate == null) {
			if (other.endDate != null) {
				return false;
			}
		} else if (!endDate.equals(other.endDate)) {
			return false;
		}
		if (Double.doubleToLongBits(measurement) != Double
				.doubleToLongBits(other.measurement)) {
			return false;
		}
		if (nuclides == null) {
			if (other.nuclides != null) {
				return false;
			}
		} else if (!nuclides.equals(other.nuclides)) {
			return false;
		}
		if (startDate == null) {
			if (other.startDate != null) {
				return false;
			}
		} else if (!startDate.equals(other.startDate)) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MeasurementSearchControls [startDate=" + startDate
				+ ", endDate=" + endDate + ", measurement=" + measurement
				+ ", constraint=" + constraint + ", nuclides=" + nuclides + "]";
	}

}

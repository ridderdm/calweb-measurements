/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice.beans;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;


import nl.bori.calweb.services.measurementsservice.beans.MeasurementsServiceBean;


import org.apache.log4j.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import com.stvconsultants.easygloss.StandardGloss;
import com.stvconsultants.easygloss.footnotes.Footnote;

/**
 * @author ridderdm
 *
 */
public class MeasurementsServiceBeanTest {


	protected static String persistenceUnitName = "stcmetingenPU";
	protected static String persistenceUnitTestName = persistenceUnitName + "-test";

	
	private static Logger logger = Logger.getLogger(MeasurementsServiceBeanTest.class);

	private static EntityManagerFactory emf;
	private EntityManager em;
	private MeasurementsServiceBean bean;


	@BeforeClass
	public static void initialise() {

		emf = Persistence.createEntityManagerFactory(persistenceUnitTestName);
		
	}
		
	@Before
	public void setUp() {
		
		logger.info("setUp() van de entitymanager(factory) en de bean.");

		em = emf.createEntityManager();
		
		bean = new MeasurementsServiceBean();
		
		StandardGloss gloss = new StandardGloss(
				new Footnote(PersistenceContext.class).with("unitName", persistenceUnitName ), em);
		gloss.apply(bean);
	}
	
	@After
	public void cleanUp() {
		logger.info("cleanUp() van de testen");
		if (em != null && em.isOpen()) em.close();

	}
	
	@AfterClass
	public static void clearClass() {
		if (emf != null && emf.isOpen()) emf.close();
	}
	
	@Test
	public void testEntityManager() {
		assertNotNull(bean.getEntityManager());
		assertTrue(bean.getEntityManager().isOpen());
	}
	

	
	
}

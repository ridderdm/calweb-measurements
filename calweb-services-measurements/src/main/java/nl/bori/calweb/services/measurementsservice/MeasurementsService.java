/**
 * $Id$
 */
package nl.bori.calweb.services.measurementsservice;

import javax.ejb.Local;

/**
 * @author ridderdm
 *
 */
@Local
public interface MeasurementsService extends MeasurementsSearchService {

}
